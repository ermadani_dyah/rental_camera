-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2021 at 06:02 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rental_kamera`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gambar` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `email`, `gambar`, `username`, `password`) VALUES
(1, 'Ermadani Dyah R', 'ermadani.dyah09@gmail.com', 'user1-128x128.jpg', 'ermadani_dyah', '8fbfd88d05ee870fbf9b12e8b42db844'),
(2, 'nana', 'nanananana', '', 'na_jaemin', '9f11b76ab43f1c60530ecb8695f7a4b0');

-- --------------------------------------------------------

--
-- Table structure for table `cabang`
--

CREATE TABLE `cabang` (
  `id_cabang` int(11) NOT NULL,
  `nama_cabang` varchar(20) NOT NULL,
  `alamat` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cabang`
--

INSERT INTO `cabang` (`id_cabang`, `nama_cabang`, `alamat`) VALUES
(1, 'Rental Sukun', 'kecamatan sukun'),
(2, 'Perusahaan NSSSS', 'dusun pucang anom desa pucang simo'),
(3, 'PERUSAHAAN C', 'dusun pucang anom desa pucang simo'),
(5, 'Rental Malang', 'dusun pucang anom desa pucang simo'),
(6, 'PERUSAHAAN SJS', 'jln malang');

-- --------------------------------------------------------

--
-- Table structure for table `jasa`
--

CREATE TABLE `jasa` (
  `id_jasa` int(11) NOT NULL,
  `nama_jasa` varchar(20) NOT NULL,
  `wilayah` varchar(20) NOT NULL,
  `harga_jasa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jasa`
--

INSERT INTO `jasa` (`id_jasa`, `nama_jasa`, `wilayah`, `harga_jasa`) VALUES
(2, 'COD Malang', 'Kabupaten Malang', 20000),
(3, 'COD Baru', 'kecamatan gadang', 20000),
(4, 'COD', 'kecamatan barangsuki', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `merek`
--

CREATE TABLE `merek` (
  `id_merek` int(11) NOT NULL,
  `nama_merek` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merek`
--

INSERT INTO `merek` (`id_merek`, `nama_merek`) VALUES
(1, 'Nikon'),
(2, 'Canon'),
(3, 'Sony');

-- --------------------------------------------------------

--
-- Table structure for table `paket`
--

CREATE TABLE `paket` (
  `id_paket` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `nama_paket` varchar(20) NOT NULL,
  `waktu` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `id_merek` int(11) NOT NULL,
  `jumlah_produk` int(11) NOT NULL,
  `jenis_paket` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paket`
--

INSERT INTO `paket` (`id_paket`, `id_produk`, `nama_paket`, `waktu`, `harga`, `id_merek`, `jumlah_produk`, `jenis_paket`, `status`) VALUES
(1, 2, 'Paket Hemat', 16, 10000, 2, 5, 'weekday', 'Tersedia'),
(21, 3, 'Paket Hemat 2', 22, 30000, 2, 0, 'weekday', 'Tidak tersedia'),
(24, 2, 'hemat', 3, 3000, 2, 3, 'weekend', 'Tersedia'),
(31, 23, 'Paket Nikon 2', 24, 40000, 1, 4, 'Weekday', 'Tersedia'),
(32, 24, 'Paket Sony', 40, 100000, 3, 4, 'Weekend', 'Tersedia');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(65) NOT NULL,
  `no_hp` bigint(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `identitas` bigint(30) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `nama`, `alamat`, `no_hp`, `email`, `identitas`, `jenis_kelamin`, `username`, `password`) VALUES
(1, 'ermadani ', 'desa pucangsimo', 81555442712, 'ermadani.dyah09@gmail.com', 3517182383372656789, 'perempuan', 'ermadani_dyah', '8fbfd88d05ee870fbf9b12e8b42db844'),
(3, 'Na Jaemin', 'soul', 81555442712, 'na.jaemin08@gmail.com', 12456678, 'laki laki', 'nana_jaemin', '8fbfd88d05ee870fbf9b12e8b42db844'),
(4, 'Lee Jeno', 'soul', 81555442712, 'lee.jeno09@gmail.com', 654567890, 'laki laki', 'lee_jeno', '8fbfd88d05ee870fbf9b12e8b42db844'),
(5, 'Park Chanyeol', 'soul', 81555442712, 'park_chanyeol09@gmail.com', 876540987654, 'laki laki', 'park_chanyeol', '8fbfd88d05ee870fbf9b12e8b42db844');

-- --------------------------------------------------------

--
-- Table structure for table `pengembalian`
--

CREATE TABLE `pengembalian` (
  `id_pengembalian` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `gambar` varchar(30) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `nama_produk` varchar(20) NOT NULL,
  `id_merek` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `gambar` varchar(65) NOT NULL,
  `spesifikasi` varchar(100) NOT NULL,
  `kelengkapan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `id_merek`, `type`, `gambar`, `spesifikasi`, `kelengkapan`) VALUES
(1, 'Canon EOS', 2, '1100D ', 'gambar1.png', 'Sensor APS-C CMOS 24,1 megapik', '-Tas -Memori 16gb -Charger'),
(2, 'Canon EOS ', 2, '1D X Mark III', 'gambar3.png', 'Sensor CMOS 20,1MP + prosesor ', '-Tas-Memori 16 Gb -Charger -Le'),
(3, 'Canon EOS ', 2, '90D (Bodi)', 'gambar4.png', 'Sensor APS-C CMOS 32,5MP + DIGIC 8\r\nDual Pixel CMOS AF\r\n45 AF tipe silang (Viewfinder Optik)', '-Tas-Memori 16 Gb -Charger -Le'),
(4, 'Canon EOS ', 2, '1D X Mark III', 'gambar 2.png', 'Sensor APS-C CMOS 32,5MP + DIGIC 8\r\nDual Pixel CMOS AF\r\n45 AF tipe silang (Viewfinder Optik)', '-Tas-Memori 16 Gb -Charger -Le'),
(20, 'Nikon Digital SLR', 1, 'D5', 'Nikon Digital SLR.jpg', 'Sensor CMOS 20,1MP + prosesor gambar DIGIC X', '-Tas-Memori 16 Gb -Charger -Le'),
(21, 'Nikon Digital SLR', 1, 'D850', 'Nikon Digital SLR.jpg', 'Sensor APS-C CMOS 32,5MP + DIGIC 8 Dual Pixel CMOS AF 45 AF tipe silang (Viewfinder Optik)', '-Tas-Memori 16 Gb -Charger -Le'),
(22, 'Nikon Digital SLR', 1, '1D X Mark III', 'Nikon Digital SLR.jpg', 'Sensor CMOS 20,1MP + prosesor gambar DIGIC X', '-Tas-Memori 16 Gb -Charger -Le'),
(23, 'Nikon Digital SLR', 1, '287273', 'Nikon Digital SLR.jpg', 'Sensor APS-C CMOS 24,1 megapiksel+ perekaman video 4K 45 titik AF semua tipe silang (jendela bidik) ', '-Tas-Memori 16 Gb -Charger -Le'),
(24, 'Sony Digital Camera', 3, '909DE', 'Sony Digital Camera.jpg', 'Sensor CMOS 20,1MP + prosesor gambar DIGIC X', '-Tas-Memori 16 Gb -Charger -Le'),
(25, 'Sony Digital Camera', 3, '1E407', 'Sony Digital Camera.jpg', 'Sensor APS-C CMOS 24,1 megapiksel+ perekaman video 4K 45 titik AF semua tipe silang (jendela bidik) ', '-Tas-Memori 16 Gb -Charger -Le'),
(26, 'Sony Digital Camera', 3, '3O8GH', 'Sony Digital Camera.jpg', 'Sensor APS-C CMOS 24,1 megapiksel+ perekaman video 4K 45 titik AF semua tipe silang (jendela bidik) ', '-Tas-Memori 16 Gb -Charger -Le'),
(27, 'Sony Digital Camera', 3, '850D (Bodi)', 'Sony Digital Camera.jpg', 'Sensor CMOS 20,1MP + prosesor gambar DIGIC X', '-Tas-Memori 16 Gb -Charger -Le');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `id_paket` int(11) NOT NULL,
  `jumlah_produk` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `lama` int(11) NOT NULL,
  `id_jasa` int(11) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `total_harga` bigint(30) NOT NULL,
  `Terlambat` int(11) NOT NULL,
  `Denda` bigint(20) NOT NULL,
  `status_transaksi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_pelanggan`, `id_paket`, `jumlah_produk`, `tanggal_pinjam`, `tanggal_kembali`, `lama`, `id_jasa`, `id_cabang`, `total_harga`, `Terlambat`, `Denda`, `status_transaksi`) VALUES
(17, 1, 24, 1, '2021-04-25', '2021-04-26', 16, 2, 2, 35000, 0, 0, 'Dipinjam');

--
-- Triggers `transaksi`
--
DELIMITER $$
CREATE TRIGGER `delate` AFTER DELETE ON `transaksi` FOR EACH ROW BEGIN

   UPDATE paket SET jumlah_produk = jumlah_produk + OLD.jumlah_produk
   WHERE id_paket = OLD.id_paket;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `transaksi_pinjaman` AFTER INSERT ON `transaksi` FOR EACH ROW BEGIN

   UPDATE paket SET jumlah_produk = jumlah_produk - NEW.jumlah_produk

   WHERE id_paket = NEW.id_paket;

END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `cabang`
--
ALTER TABLE `cabang`
  ADD PRIMARY KEY (`id_cabang`);

--
-- Indexes for table `jasa`
--
ALTER TABLE `jasa`
  ADD PRIMARY KEY (`id_jasa`);

--
-- Indexes for table `merek`
--
ALTER TABLE `merek`
  ADD PRIMARY KEY (`id_merek`);

--
-- Indexes for table `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id_paket`),
  ADD KEY `id_merek` (`id_merek`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD PRIMARY KEY (`id_pengembalian`),
  ADD KEY `id_transaksi` (`id_transaksi`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD KEY `id_merek` (`id_merek`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_pelanggan` (`id_pelanggan`),
  ADD KEY `id_paket` (`id_paket`),
  ADD KEY `id_jasa` (`id_jasa`),
  ADD KEY `id_cabang` (`id_cabang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cabang`
--
ALTER TABLE `cabang`
  MODIFY `id_cabang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jasa`
--
ALTER TABLE `jasa`
  MODIFY `id_jasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `merek`
--
ALTER TABLE `merek`
  MODIFY `id_merek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `paket`
--
ALTER TABLE `paket`
  MODIFY `id_paket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pengembalian`
--
ALTER TABLE `pengembalian`
  MODIFY `id_pengembalian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `paket`
--
ALTER TABLE `paket`
  ADD CONSTRAINT `paket_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `paket_ibfk_2` FOREIGN KEY (`id_merek`) REFERENCES `merek` (`id_merek`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD CONSTRAINT `pengembalian_ibfk_1` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id_transaksi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`id_merek`) REFERENCES `merek` (`id_merek`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id_paket`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`id_jasa`) REFERENCES `jasa` (`id_jasa`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_4` FOREIGN KEY (`id_cabang`) REFERENCES `cabang` (`id_cabang`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
