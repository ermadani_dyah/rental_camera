<!DOCTYPE html>
<?php
	include("Admin/koneksi/koneksi.php");
	session_start();
	if(isset($_GET["include"])){
		$include = $_GET["include"];
		if($include=="konfirmasi_login"){
				include("include/konfirmasi_login.php");
		}else if($include=="konfirmasi_tambah_pelanggan"){
			include("include/konfirmasi_tambah_pelanggan.php");
		}else if($include=="konfirmasi_tambah_pengembalian"){
			include("include/konfirmasi_tambah_pengembalian.php");
		}else if($include=="konfirmasi_tambah_transaksi"){
			include("include/konfirmasi_tambah_transaksi.php");
		}else if($include=="konfirmasi_login_profil"){
			include("include/konfirmasi_login_profil.php");
		}else if($include=="Logout"){
			include("include/Logout.php");
		}
	 }
?>
<html>
	<?php
		if(isset($_GET["include"])){
			$include = $_GET["include"];
	?>
			<head>
				<?php include("includes/head.php")?>
			</head>
			<body> 
				<div class="container">
				<!-- Top box -->
					<?php include("includes/navbar.php")?>
					<main>
						<?php
							if($include=="Cabang"){
								include("include/Cabang.php");
							}else if($include=="Status"){
								include("include/Status.php");
							}else if($include=="Transaksi"){
								include("include/Transaksi.php");
							}else if($include=="PricelistCanon"){
								include("include/PricelistCanon.php");
							}else if($include=="PricelistNikon"){
								include("include/PricelistNikon.php");
							}else if($include=="PricelistSony"){
								include("include/PricelistSony.php");
							}else if($include=="Detail_Transaksi"){
								include("include/Detail_Transaksi.php");
							}else if($include=="Pengembalian"){
								include("include/Pengembalian.php");
							}else if($include=="Sign_up"){
								include("include/Sign_up.php");
							}
						?>
					</main>
					<?php include("includes/footer.php")?>
				</div>
				<?php include("includes/script.php")?>
			</body>
	<?php
		}else if(isset($_GET['LO'])){
			$include = $_GET["LO"];
	?>
				<head>
					<?php include("includes/head_login.php");?>
				</head>
	<?php
			if($include=="Login"){
				include("include/Login.php");
			}else if($include=="Login_profil"){
				include("include/Login_profil.php");
			}
		}else if(isset($_GET['PRO'])){
			$include = $_GET["PRO"];
	?>
				<head>
				<?php include("includes/head.php");?>
			</head>
			<body> 
				<div class="container">
				<!-- Top box -->
					<?php include("includes/navbar_profil.php")?>
					<main>
						<?php 
							if($include=="Profil"){
								include("include/Profil.php");
							}
						?>
					</main>
					<?php include("includes/footer.php")?>
				</div>
				<?php include("includes/script.php")?>
			</body>
	<?php
		} else{
	?>
			<head>
				<?php include("includes/head.php");?>
			</head>
			<body> 
				<div class="container">
				<!-- Top box -->
					<?php include("includes/navbar.php")?>
					<main>
						<?php include("include/Home.php");?>
					</main>
					<?php include("includes/footer.php")?>
				</div>
				<?php include("includes/script.php")?>
			</body>
	<?php
		}
	?>
</html>