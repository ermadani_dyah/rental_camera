<?php 
	include("koneksi/koneksi.php");
	session_start();
	if(isset($_GET["include"])){
		$include = $_GET["include"];
		if($include=="konfirmasi_login"){
			include("include/konfirmasi_login.php");
		}else if($include=="singout"){
			include("include/singout.php");
		}else if($include=="konfirmasi_tambah_cabang"){
			include("include/konfirmasi_tambah_cabang.php");
		}else if($include=="konfirmasi_edit_cabang"){
			include("include/konfirmasi_edit_cabang.php");
		}else if($include=="konfirmasi_tambah_pelanggan"){
			include("include/konfirmasi_tambah_pelanggan.php");
		}else if($include=="konfirmasi_edit_pelanggan"){
			include("include/konfirmasi_edit_pelanggan.php");
		}else if($include=="konfirmasi_tambah_jasa"){
			include("include/konfirmasi_tambah_jasa.php");
		}else if($include=="konfirmasi_edit_jasa"){
			include("include/konfirmasi_edit_jasa.php");
		}else if($include=="konfirmasi_tambah_produk"){
			include("include/konfirmasi_tambah_produk.php");
		}else if($include=="konfirmasi_edit_produk"){
			include("include/konfirmasi_edit_produk.php");
		}else if($include=="konfirmasi_tambah_pricelist"){
			include("include/konfirmasi_tambah_pricelist.php");
		}else if($include=="konfirmasi_edit_pricelist"){
			include("include/konfirmasi_edit_pricelist.php");
		}
		
	}
?>
<html>
	<head>
		<?php include("includes/head.php")?>
	</head>
	<?php
		//jika ada get include
		if(isset($_GET["include"])){
		  $include = $_GET["include"];
		  //jika ada session id admin
			if(isset($_SESSION['id_admin'])){
			//pemanggilan ke halaman-halaman menu admin
	?>
				<body class="hold-transition sidebar-mini layout-fixed">
					<div class="wrapper">
					<!-- Loading -->
					<!-- Navbar -->
					<?php include("includes/navbar.php")?>
					<!-- /.navbar -->
					<!-- Sidebar -->
					<?php include("includes/sidebar.php")?>
					<!-- Content Wrapper. Contains page content -->
					<?php
						if($include=="dashboard"){
							include("include/dashboard.php");
						}else if($include=="Cabang"){
							include("include/Cabang.php");
						}else if($include=="tambah_cabang"){
							include("include/tambah_cabang.php");
						}else if($include=="edit_cabang"){
							include("include/edit_cabang.php");
						}else if($include=="Pelanggan"){
							include("include/Pelanggan.php");
						}else if($include=="tambah_pelanggan"){
							include("include/tambah_pelanggan.php");
						}else if($include=="edit_pelanggan"){
							include("include/edit_pelanggan.php");
						}else if($include=="detail_pelanggan"){
							include("include/detail_pelanggan.php");
						}else if($include=="Jasa"){
							include("include/Jasa.php");
						}else if($include=="tambah_jasa"){
							include("include/tambah_jasa.php");
						}else if($include=="edit_jasa"){
							include("include/edit_jasa.php");
						}else if($include=="Produk"){
							include("include/Produk.php");
						}else if($include=="tambah_produk"){
							include("include/tambah_produk.php");
						}else if($include=="edit_produk"){
							include("include/edit_produk.php");
						}else if($include=="detail_produk"){
							include("include/detail_produk.php");
						}else if($include=="Pricelist"){
							include("include/Pricelist.php");
						}else if($include=="tambah_pricelist"){
							include("include/tambah_pricelist.php");
						}else if($include=="edit_pricelist"){
							include("include/edit_pricelist.php");
						}else if($include=="Transaksi"){
							include("include/Transaksi.php");
						}else if($include=="detail_transaksi"){
							include("include/detail_transaksi.php");
						}else if($include=="Pengembalian"){
							include("include/Pengembalian.php");
						}else if($include=="profil"){
							include("include/profil.php");
						}
					?>
					<!-- /.content-wrapper -->

					<!--Footer-->
					<?php include("includes/footer.php")?>

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar -->
					</div>
				<!-- ./wrapper -->
				</body>
	<?php    
			}
		}else{
			//jika tidak ada include pemanggilan halaman form login
			include("include/login.php");
		}
	?>
	<!------ Include the above in your HEAD tag ---------->
	<?php include("includes/script.php")?>
</html>