<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!--Logo -->
    <a href="index3.html" class="brand-link">
        <img src="dist/img/lOGO.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Kamera Kuy</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="index.php?include=dashboard" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="index.php?include=Cabang" class="nav-link">
                    <i class="nav-icon fas fa-building"></i>
                    <p>Cabang Toko</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="index.php?include=Pelanggan" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>Pelanggan</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="index.php?include=Produk" class="nav-link">
                        <i class="nav-icon fas fa-camera"></i>
                        <p>Produk</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="index.php?include=Pricelist" class="nav-link">
                        <i class="nav-icon fas fa-clipboard-list"></i>
                        <p>Pricelist</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="index.php?include=Jasa" class="nav-link">
                        <i class="nav-icon fas fa-truck-loading"></i>
                        <p>Jasa</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="index.php?include=Transaksi" class="nav-link">
                        <i class="nav-icon fas fa-cash-register"></i>
                        <p>Transaksi</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="index.php?include=Pengembalian" class="nav-link">
                        <i class="nav-icon fas fa-hand-holding-usd"></i>
                        <p>Pengembalian</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>