<?php 
	if(isset($_SESSION['id_produk'])){
        $id_produk=$_SESSION['id_produk'];
		$nama_produk = $_POST['nama_produk'];
        $merek = $_POST['merek'];
        $type = $_POST['type'];
        $spesifikasi = $_POST['spesifikasi'];
        $kelengkapan = $_POST['kelengkapan'];
        $_SESSION['nama_produk']=$nama_produk;
        $_SESSION['merek']=$merek;
        $_SESSION['type']=$type;
        $_SESSION['spesifikasi']=$spesifikasi;
        $_SESSION['kelengkapan']=$kelengkapan;
        if(empty($nama_produk)){
            header("Location:index.php?include=edit_produk&data=".$id_produk."&notif=editkosong&jenis=Nama Produk");
        }else if(empty($merek)){
            header("Location:index.php?include=edit_produk&data=".$id_produk."&notif=editkosong&jenis=Merek Produk");
        }else if(empty($type)){
            header("Location:index.php?include=edit_produk&data=".$id_produk."&notif=editkosong&jenis=Type Produk");
        }else if(empty($spesifikasi)){
            header("Location:index.php?include=edit_produk&data=".$id_produk."&notif=editkosong&jenis=Spesifikasi Produk");
        }else if(empty($kelengkapan)){
            header("Location:index.php?include=edit_produk&data=".$id_produk."&notif=editkosong&jenis=Kelengkapan Produk");
        }else{
            $lokasi_file = $_FILES['foto']['tmp_name'];
            $nama_file = $nama_produk.".jpg";
            $direktori = 'dist/img/'.$type;
            if(move_uploaded_file($lokasi_file,$direktori)){
                $sql = "update `produk` set `nama_produk`='$nama_produk',`id_merek`='$merek',`type`='$type', `gambar`='$nama_file',`spesifikasi`='$spesifikasi',`kelengkapan`='$kelengkapan' where `id_produk` = '$id_produk'";
                mysqli_query($koneksi,$sql);
            }else{
                $sql = "update `produk` set `nama_produk`='$nama_produk',`id_merek`='$merek',`type`='$type',`spesifikasi`='$spesifikasi',`kelengkapan`='$kelengkapan' where `id_produk` = '$id_produk'";
                mysqli_query($koneksi,$sql);
            }
            header("Location:index.php?include=Produk&notif=editberhasil");
            unset($_SESSION['nama_produk']);
            unset($_SESSION['merek']);
            unset($_SESSION['type']);
            unset($_SESSION['spesifikasi']);
            unset($_SESSION['kelengkapan']);
        }
	}
?>