<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <?php 
                $sql = "select * FROM produk";
                $query = mysqli_query($koneksi,$sql);
                $data  =  mysqli_num_rows ($query)  ; 
                
              ?>
              <h3><?php echo $data?></h3>
              <p>Produk</p>
            </div>
            <div class="icon">
              <i class="ion ion-camera"></i>
            </div>
            <a href="index.php?include=Produk" class="small-box-footer">
              More info 
              <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <?php 
                $sql = "select * FROM transaksi";
                $query = mysqli_query($koneksi,$sql);
                $data  =  mysqli_num_rows ($query)  ; 
                
              ?>
              <h3><?php echo $data?></h3>
              <p>Transaksi</p>
            </div>
            <div class="icon">
              <i class="ion ion-podium"></i>
            </div>
            <a href="index.php?include=Transaksi" class="small-box-footer">
              More info 
              <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
            <?php 
                $sql = "select * FROM pelanggan";
                $query = mysqli_query($koneksi,$sql);
                $data  =  mysqli_num_rows ($query)  ; 
                
              ?>
              <h3><?php echo $data?></h3>
              <p>Pelanggan</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="index.php?include=Pelanggan" class="small-box-footer">
              More info 
              <i class="fas fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
            <?php 
                $sql = "select * FROM cabang";
                $query = mysqli_query($koneksi,$sql);
                $data  =  mysqli_num_rows ($query)  ; 
                
              ?>
              <h3><?php echo $data?></h3>
              <p>Cabang</p>
            </div>
            <div class="icon">
              <i class="ion ion-location"></i>
            </div>
            <a href="index.php?include=Cabang" class="small-box-footer">
              More info 
              <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <div class="col-md-6">
          <!-- Diagram -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Transaksi</h3>
            </div>
            <div class="card-body">
              <div class="chart">
                <canvas id="areaChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- ./Diagram -->
        </div>
        <div class="col-md-6">
          <!-- CHART -->
          <div class="card card-danger">
            <div class="card-header">
              <h3 class="card-title">Produk</h3>
            </div>
            <div class="card-body">
              <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.Chart -->
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

