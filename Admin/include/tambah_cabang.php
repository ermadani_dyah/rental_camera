<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Tambah Cabang Perusahaan</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="index.php?include=Cabang">Cabang</a>
            </li>
            <li class="breadcrumb-item active">Tambah Cabang</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <div class="row">
          <!-- column -->
          <div class="col-sm-12">
              <div class="card">
                  <div class="row">
                      <!-- column -->
                      <div class="col-sm-12">
                          <div class="card-body">
                              <section class="content">
                                  <div class="card card-info">
                                      <div class="card-header">
                                          <h3 class="card-title" style="margin-top:5px;">
                                              <i class="far fa-list-alt"></i> Form Tambah Cabang
                                          </h3>
                                      </div>
                                      <!-- /.card-header -->
                                      <!-- form start -->
                                      <br>
                                      <div class="col-sm-10">
                                          <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                                              <?php if($_GET['notif']=="tambahkosong"){?>
                                                  <div class="alert alert-danger" role="alert">Maaf data
                                                      <?php echo $_GET['jenis'];?> wajib di isi
                                                  </div>
                                              <?php }?>
                                          <?php }?>
                                      </div>
                                      <form class="form-horizontal" enctype="multipart/form-data" method="post" action="index.php?include=konfirmasi_tambah_cabang">
                                          <div class="card-body">
                                              <div class="form-group row">
                                                  <label for="hobi" class="col-sm-3 col-form-label">Nama Cabang</label>
                                                  <div class="col-sm-7">
                                                      <input type="text" name="nama_cabang" class="form-control" id="nama_cabang" value="<?php if(!empty($_SESSION['nama_cabang'])){ echo $_SESSION['nama_cabang'];} ?>">
                                                  </div>
                                              </div>
                                              <div class="form-group row">
                                                  <label for="hobi" class="col-sm-3 col-form-label">Alamat Cabang</label>
                                                  <div class="col-sm-7">
                                                      <input type="text" name="alamat_cabang" class="form-control" id="alamat_cabang" value="<?php if(!empty($_SESSION['alamat_cabang'])){ echo $_SESSION['alamat_cabang'];} ?>">
                                                  </div>
                                              </div>
                                          </div>
                                          <!-- /.card-body -->
                                          <div class="card-footer">
                                              <button type="submit" class="btn btn-info float-right">
                                                  <i class="mr-3 fas fa-plus" aria-hidden="true"></i>Tambah
                                              </button>
                                              <div class="text-left upgrade-btn">
                                                  <a href="index.php?include=Cabang" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                                                      <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                      Kembali
                                                  </a>
                                              </div>
                                          </div>
                                          <!-- /.card-footer -->
                                      </form>
                                  </div>
                                  <!-- /.card -->
                              </section>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- /.content -->
</div>