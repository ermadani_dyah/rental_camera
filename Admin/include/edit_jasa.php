<?php 
    if(isset($_GET['data'])){
		$id_jasa = $_GET['data'];
		$_SESSION['id_jasa']=$id_jasa;
		//get data jasa
		$sql_m = "select * from `jasa` where `id_jasa` = '$id_jasa'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$id_jasa= $data_m[0];
			$nama_jasa = $data_m[1];
			$wilayah= $data_m[2];
            $harga= $data_m[3];
		} 
	}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Edit Data Jasa</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="index.php?include=Jasa">Jasa</a>
            </li>
            <li class="breadcrumb-item active">Edit Jasa</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <div class="row">
          <!-- column -->
          <div class="col-sm-12">
              <div class="card">
                  <div class="row">
                      <!-- column -->
                      <div class="col-sm-12">
                          <div class="card-body">
                              <section class="content">
                                  <div class="card card-info">
                                      <div class="card-header">
                                          <h3 class="card-title" style="margin-top:5px;">
                                              <i class="far fa-list-alt"></i> Form Edit Data Jasa
                                          </h3>
                                      </div>
                                      <!-- /.card-header -->
                                      <!-- form start -->
                                      <br>
                                      <div class="col-sm-10">
                                          <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                                              <?php if($_GET['notif']=="editkosong"){?>
                                                  <div class="alert alert-danger" role="alert">
                                                      Maaf data 
                                                      <?php echo $_GET['jenis'];?> wajib di isi
                                                  </div>
                                              <?php }?>
                                          <?php }?>
                                      </div>
                                      <form class="form-horizontal" enctype="multipart/form-data" method="post" action="index.php?include=konfirmasi_edit_jasa">
                                          <div class="card-body">
                                              <div class="form-group row">
                                                  <label for="hobi" class="col-sm-3 col-form-label">Nama Jasa</label>
                                                  <div class="col-sm-7">
                                                      <input type="text" name="nama_jasa"  class="form-control" id="nama_jasa" value="<?php echo $nama_jasa;?>">
                                                  </div>
                                              </div>
                                              <div class="form-group row">
                                                  <label for="hobi" class="col-sm-3 col-form-label">Wilayah</label>
                                                  <div class="col-sm-7">
                                                      <input type="text" name="wilayah"  class="form-control" id="wilayah" value="<?php echo $wilayah;?>">
                                                  </div>
                                              </div>
                                              <div class="form-group row">
                                                  <label for="hobi" class="col-sm-3 col-form-label">Harga</label>
                                                  <div class="col-sm-7">
                                                      <input type="number" name="harga"  class="form-control" id="harga" value="<?php echo $harga;?>">
                                                  </div>
                                              </div>
                                          </div>
                                          <!-- /.card-body -->
                                          <div class="card-footer">
                                              <button type="submit" class="btn btn-info float-right">
                                                  <i class="mr-3  far fa-save" aria-hidden="true"></i>Simpan
                                              </button>
                                              <div class="text-left upgrade-btn">
                                                  <a href="index.php?include=Jasa" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                                                      <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                      Kembali
                                                  </a>
                                              </div>
                                          </div>
                                          <!-- /.card-footer -->
                                      </form>
                                  </div>
                                  <!-- /.card -->
                              </section>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- /.content -->
</div>
