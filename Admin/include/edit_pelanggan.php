<?php 
    if(isset($_GET['data'])){
		$id_pelanggan = $_GET['data'];
		$_SESSION['id_pelanggan']=$id_pelanggan;
		//get data jasa
		$sql_m = "select * from `pelanggan` where `id_pelanggan` = '$id_pelanggan'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$id_pelanggan = $data_m[0];
            $nama_pelanggan = $data_m[1];
            $alamat = $data_m[2];
            $hp = $data_m[3];
            $email = $data_m[4];
            $identitas = $data_m[5];
            $jenis_kelamin= $data_m[6];
            $username= $data_m[7];
            $password= $data_m[8];
		} 
	}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Edit Data Jasa</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="index.php?include=Pelanggan">Pelanggan</a>
            </li>
            <li class="breadcrumb-item active">Edit Pelanggan</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <div class="row">
          <!-- column -->
          <div class="col-sm-12">
              <div class="card">
                  <div class="row">
                      <!-- column -->
                      <div class="col-sm-12">
                          <div class="card-body">
                              <section class="content">
                                  <div class="card card-info">
                                      <div class="card-header">
                                          <h3 class="card-title" style="margin-top:5px;">
                                              <i class="far fa-list-alt"></i> Form Edit Data Pelanggan
                                          </h3>
                                      </div>
                                      <!-- /.card-header -->
                                      <!-- form start -->
                                      <br>
                                      <div class="col-sm-10">
                                          <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                                              <?php if($_GET['notif']=="editkosong"){?>
                                                  <div class="alert alert-danger" role="alert">
                                                      Maaf data 
                                                      <?php echo $_GET['jenis'];?> wajib di isi
                                                  </div>
                                              <?php }?>
                                          <?php }?>
                                      </div>
                                      <form class="form-horizontal" enctype="multipart/form-data" method="post" action="index.php?include=konfirmasi_edit_pelanggan">
                                          <div class="card-body">
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Nama Pelanggan</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="nama_pelanggan"  class="form-control" id="nama_pelanggan" value="<?php echo $nama_pelanggan;?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Alamat</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="alamat"  class="form-control" id="alamat" value="<?php echo $alamat;?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Nomer HP</label>
                                                    <div class="col-sm-7">
                                                        <input type="number" name="no_hp"  class="form-control" id="no_hp" value="<?php echo $hp;?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Email</label>
                                                    <div class="col-sm-7">
                                                        <input type="email" name="email"  class="form-control" id="email" value="<?php echo $email;?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">No Identitas KTP</label>
                                                    <div class="col-sm-7">
                                                        <input type="number" name="identitas"  class="form-control" id="identitas" value="<?php echo $identitas;?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="jenis_kelamin"  class="form-control" id="jenis_kelamin" value="<?php echo $jenis_kelamin;?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Username</label>
                                                    <div class="col-sm-7">
                                                        <input type="username" name="username"  class="form-control" id="username" value="<?php echo $username;?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Password</label>
                                                    <div class="col-sm-7">
                                                        <input type="password" name="password"  class="form-control" id="password" value="<?php echo $password;?>">
                                                    </div>
                                                </div>
                                          </div>
                                          <!-- /.card-body -->
                                          <div class="card-footer">
                                              <button type="submit" class="btn btn-info float-right">
                                                  <i class="mr-3  far fa-save" aria-hidden="true"></i>Simpan
                                              </button>
                                              <div class="text-left upgrade-btn">
                                                  <a href="index.php?include=Produk" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                                                      <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                      Kembali
                                                  </a>
                                              </div>
                                          </div>
                                          <!-- /.card-footer -->
                                      </form>
                                  </div>
                                  <!-- /.card -->
                              </section>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- /.content -->
</div>
