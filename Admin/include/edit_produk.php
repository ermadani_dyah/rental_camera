<?php 
	if(isset($_GET['data'])){
		$id_produk = $_GET['data'];
        $_SESSION['id_produk']=$id_produk;
		//get data produk
		$sql_m = "select `p`.`id_produk`, `p`.`nama_produk`, `m`.`id_merek`,`p`.`type`,`p`.`gambar`,`p`.`spesifikasi`,
        `p`.`kelengkapan` from `produk` `p`inner join `merek` `m` on `p`.`id_merek` = `m`.`id_merek` WHERE `p`.`id_produk`='$id_produk'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$id_produk = $data_m[0];
            $nama_produk = $data_m[1];
            $merek = $data_m[2];
            $type = $data_m[3];
            $gambar= $data_m[4];
            $spesifikasi = $data_m[5];
            $kelengkapan= $data_m[6];
		}	
	}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Edit Data Produk</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="index.php?include=Produk">Produk</a>
            </li>
            <li class="breadcrumb-item active">Edit Produk</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <div class="row">
          <!-- column -->
          <div class="col-sm-12">
              <div class="card">
                  <div class="row">
                      <!-- column -->
                      <div class="col-sm-12">
                          <div class="card-body">
                              <section class="content">
                                  <div class="card card-info">
                                      <div class="card-header">
                                          <h3 class="card-title" style="margin-top:5px;">
                                              <i class="far fa-list-alt"></i> Form Edit Data Produk
                                          </h3>
                                      </div>
                                      <!-- /.card-header -->
                                      <!-- form start -->
                                      <br>
                                      <div class="col-sm-10">
                                          <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                                              <?php if($_GET['notif']=="editkosong"){?>
                                                  <div class="alert alert-danger" role="alert">
                                                      Maaf data 
                                                      <?php echo $_GET['jenis'];?> wajib di isi
                                                  </div>
                                              <?php }?>
                                          <?php }?>
                                      </div>
                                      <form class="form-horizontal" enctype="multipart/form-data" method="post" action="index.php?include=konfirmasi_edit_produk">
                                          <div class="card-body">
                                                <div class="form-group row">
                                                        <label for="hobi" class="col-sm-3 col-form-label">Nama Produk</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" name="nama_produk"  class="form-control" id="nama_produk" value="<?php echo $nama_produk;?>">
                                                        </div>
                                                </div>
                                                <div class="form-group row">
                                                        <label for="hobi" class="col-sm-3 col-form-label">Merek</label>
                                                        <div class="col-sm-7">
                                                            <select class="form-control" id="merek" name="merek">
                                                                <option value="0">- Pilih Merek -</option>
                                                                <?php
                                                                    $sql_j = "select `id_merek`, `nama_merek` from `merek` order by `nama_merek";
                                                                    $query_j = mysqli_query($koneksi,$sql_j);
                                                                    while($data_j = mysqli_fetch_row($query_j)){
                                                                        $id_merek = $data_j[0];
                                                                        $nama_merek = $data_j[1];
                                                                ?>
                                                                <option value="<?php echo $id_merek;?>"<?php if($id_merek==$merek){?> selected="selected" <?php }?>>
                                                                    <?php echo $nama_merek;?><?php }?>
                                                                </option>
                                                            </select>
                                                        </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Type</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="type"  class="form-control" id="type" value="<?php echo $type;?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="foto" class="col-sm-3 col-form-label">Gambar </label>
                                                    <div class="col-sm-7">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input"name="foto" id="customFile">
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Spesifikasi</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="spesifikasi"  class="form-control" id="spesifikasi" value="<?php echo $spesifikasi;?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Kelengkapan</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="kelengkapan"  class="form-control" id="kelengkapan" value="<?php echo $kelengkapan;?>">
                                                    </div>
                                                </div>
                                          </div>
                                          <!-- /.card-body -->
                                          <div class="card-footer">
                                              <button type="submit" class="btn btn-info float-right">
                                                  <i class="mr-3  far fa-save" aria-hidden="true"></i>Simpan
                                              </button>
                                              <div class="text-left upgrade-btn">
                                                  <a href="index.php?include=Produk" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                                                      <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                      Kembali
                                                  </a>
                                              </div>
                                          </div>
                                          <!-- /.card-footer -->
                                      </form>
                                  </div>
                                  <!-- /.card -->
                              </section>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- /.content -->
</div>
