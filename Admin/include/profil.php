<?php 
	if(isset($_SESSION['id_admin'])){
	$id_admin = $_SESSION['id_admin'];
	//get profil
	$sql = "select `nama`, `email`,`gambar` from `admin` where `id_admin`='$id_admin'";
	 //echo $sql;
	$query = mysqli_query($koneksi, $sql);
	while($data = mysqli_fetch_row($query)){
		$nama = $data[0];
		$email = $data[1];
		$foto = $data[2];
	}
?>
    <!-- Main content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0">Profil</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                <a href="index.php">Home</a>
                </li>
                <li class="breadcrumb-item active">Profil</li>
            </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
							<div class="card-body">
								<div style="margin-bottom:0px; margin-left : 300px;">
									<img src ="dist/img/<?php echo $foto;?>"  class="img-circle img" style="border : 3px solid DarkGray; width : 200px;">
								</div><br>
								<table class="table table-bordered">
									<tbody>  
											<td width="20%"><strong>Nama<strong></td>
											<td width="80%"><?php echo $nama;?></td>
										</tr>                
										<tr>
											<td width="20%"><strong>Email<strong></td>
											<td width="80%"><?php echo $email;?></td>
										</tr>
									</tbody>
								</table>  
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
</div>
<?php } ?>