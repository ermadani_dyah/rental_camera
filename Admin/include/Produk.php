<!DOCTYPE html>
<?php 
    if((isset($_GET['aksi']))&&(isset($_GET['data']))){
		if($_GET['aksi']=='hapus'){
			$id_produk = $_GET['data'];
			//hapus cabang
			$sql_dh = "delete from `produk` where `id_produk` = '$id_produk'";
		    mysqli_query($koneksi,$sql_dh);
		}
	}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0">Produk</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <a href="index.php">Home</a>
                    </li>
                    <li class="breadcrumb-item active">Produk</li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card-body">
                                <div class="card-header">
                                    <h3 class="card-title" style="margin-top:5px;">
                                        <i class="mr-3 fas fa-camera-retro"></i> Daftar Produk
                                    </h3>
                                    <div class="card-tools">
                                        <a href="index.php?include=tambah_produk" class="btn btn-sm btn-info float-right">
                                            <i class="fas fa-plus"></i> Tambah Daftar Produk
                                        </a>
                                    </div>
                                </div> 
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <form method="post" action="index.php?include=Produk">
                                            <div class="row">
                                                <div class="col-md-4 bottom-10">
                                                    <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Nama Produk & Type">
                                                </div>
                                                <div class="col-md-5 bottom-10">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="mr-3 fas fa-search"aria-hidden="true"></i>&nbsp; Search
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- .row -->
                                        </form>
                                    </div>
                                    <br>
                                    <div class="col-sm-12">
                                        <?php if(!empty($_GET['notif'])){?>
                                            <?php if($_GET['notif']=="tambahberhasil"){?>
                                                <div class="alert alert-success" role="alert">
                                                    Data Berhasil Ditambahkan
                                                </div>
                                            <?php } else if($_GET['notif']=="editberhasil"){?>
                                                <div class="alert alert-success" role="alert">
                                                    Data Berhasil Diubah
                                                </div>
                                            <?php }?>
                                        <?php }?>
                                    </div>
                                    <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                                <th width="5%">NO</th>
                                                <th width="15%">Nama</th>
                                                <th width="15%">Merek</th>
                                                <th width="15%">Type</th>
                                                <th width="15%">Spesifikasi</th>
                                                <th width="20%">
                                                    <center>Aksi</center>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                //menampilkan data hobi
                                                $batas = 3;
                                                if(!isset($_GET['halaman'])){
                                                    $posisi = 0;
                                                    $halaman = 1;	
                                                }else{
                                                    $halaman = $_GET['halaman'];
                                                    $posisi = ($halaman-1) * $batas;
                                                }
                                                $x = $posisi+1;
                                                $sql_h = "select `p`.`id_produk`, `p`.`nama_produk`, `m`.`nama_merek`,`p`.`type`,`p`.`gambar`,`p`.`spesifikasi`,`p`.`kelengkapan` from `produk` `p`inner join `merek` `m` on `p`.`id_merek` = `m`.`id_merek`"; 
                                                if (isset($_POST["katakunci"])){
                                                    $katakunci_produk = $_POST["katakunci"];
                                                    $_SESSION['katakunci_produk'] = $katakunci_produk;
                                                    $sql_h .= " where `p`.`nama_produk` LIKE '%$katakunci_produk%' OR `p`.`type` LIKE '%$katakunci_produk%' OR `m`.`nama_merek` LIKE '%$katakunci_produk%' ";
                                                } 
                                                $sql_h .= "order by `nama_produk` limit $posisi, $batas";
                                                $query_h = mysqli_query($koneksi,$sql_h);
                                                $no=$posisi+1;
                                                while($data_h = mysqli_fetch_row($query_h)){
                                                    $id_produk = $data_h[0];
                                                    $nama_produk = $data_h[1];
                                                    $merek = $data_h[2];
                                                    $type = $data_h[3];
                                                    $gambar= $data_h[4];
                                                    $spesifikasi = $data_h[5];
                                                    $kelengkapan= $data_h[6];
                                            ?>	
                                            <tr>
                                                <td><?php echo $no;?></td>
                                                <td><?php echo $nama_produk;?></td>
                                                <td><?php echo $merek;?></td>
                                                <td><?php echo $type;?></td>
                                                <td><?php echo $spesifikasi;?></td>
                                                <td align="center">
                                                    <a href="index.php?include=edit_produk&data=<?php echo $id_produk;?>" class="btn btn-xs btn-info">
                                                        <i class="fas fa-edit"></i> Edit
                                                    </a>
                                                    <a href="javascript:if(confirm('Anda yakin ingin menghapus data <?php echo $nama_produk; ?>?'))window.location.href = 'index.php?include=Produk&aksi=hapus&data=<?php echo $id_produk;?>'" class="btn btn-xs btn-warning">
                                                        <i class="fas fa-trash"></i> Hapus 
                                                    </a>
                                                    <a href="index.php?include=detail_produk&data=<?php echo $id_produk;?>" class="btn btn-xs bg-success" title="Detail">
                                                        <i class="fas fa-eye"></i>  Detail 
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                                $no++;
                                            }?>
                                            <?php 
                                                //hitung jumlah semua data 
                                                $sql_jum = "select `p`.`id_produk`, `p`.`nama_produk`, `m`.`nama_merek`,`p`.`type`,`p`.`gambar`,`p`.`spesifikasi`,`p`.`kelengkapan` from `produk` `p`inner join `merek` `m` on `p`.`id_merek` = `m`.`id_merek`"; 
                                                if (isset($_SESSION['katakunci_produk'])){
                                                    $katakunci_produk = $_SESSION['katakunci_produk'];
                                                    $sql_jum .= "  where `p`.`nama_produk` LIKE '%$katakunci_produk%' OR `p`.`type` LIKE '%$katakunci_produk%' OR `m`.`nama_merek` LIKE '%$katakunci_produk%'";
                                                } 
                                                $sql_jum .= " order by `nama_produk`";
                                                $query_jum = mysqli_query($koneksi,$sql_jum);
                                                $jum_data = mysqli_num_rows($query_jum);
                                                $jum_halaman = ceil($jum_data/$batas);
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <span>Showing <?php if($jum_data>0){echo $x;}else {echo 0;}?> to <?php echo $no-1;?> of <?php echo $jum_data;?> entries</span>
                                    <ul class="pagination pagination-sm m-0 float-right">
                                        <?php 
                                            if($jum_halaman==0){
                                                //tidak ada halaman
                                            }else if($jum_halaman==1){
                                                echo "<li class='page-item'><a class='page-link'>1</a></li>";
                                            }else{
                                                $sebelum = $halaman-1;
                                                $setelah = $halaman+1;                  
                                                if($halaman!=1){
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Produk&halaman=1'>First</a></li>";
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Produk&halaman=$sebelum'>«</a></li>";
                                                }
                                                //menampilkan angka halaman
                                                for($i=1; $i<=$jum_halaman; $i++){
                                                    if($i!=$halaman){
                                                        echo "<li class='page-item'><a class='page-link' href='index.php?include=Produk&halaman=$i'>$i</a></li>";
                                                    }else{
                                                        echo "<li class='page-item'><a class='page-link'>$i</a></li>";
                                                    }
                                                }
                                                if($halaman!=$jum_halaman){
                                                    echo "<li class='page-item'><a class='page-link'  href='index.php?include=Produk&halaman=$setelah'>»</a></li>";
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Produk&halaman=$jum_halaman'>Last</a></li>";
                                                }
                                            }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
</div>
