<?php 
	if(isset($_GET['data'])){
		$id_transaksi = $_GET['data'];
		//get data produk
		$sql_m = "select `T`.`id_transaksi`,`P`.`nama`,`Pa`.`nama_paket`,`Pa`.`harga`,`T`.`jumlah_produk`,
        `T`.`tanggal_pinjam`,`T`.`tanggal_kembali`,`T`.`lama`,`J`.`nama_jasa`,`J`.`harga_jasa`,`C`.`nama_cabang`,
        `T`.`total_harga`,`T`.`terlambat`,`T`.`denda`,`T`.`status_transaksi` ,`Pa`.`waktu` from `transaksi` `T` inner join `pelanggan` `P`
        ON `T`.`id_pelanggan`=`P`.`id_pelanggan` inner join `Paket` `Pa` ON `T`.`id_paket`=`Pa`.`id_paket` inner join
        `jasa` `J` ON `T`.`id_jasa`=`J`.`id_jasa` inner join `cabang` `C` ON `T`.`id_cabang`=`C`.`id_cabang` WHERE `T`.`id_transaksi` ='$id_transaksi'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_h = mysqli_fetch_array($query_m)){
            $id_transaksi = $data_h['id_transaksi'];
            $nama_pelanggan = $data_h['nama'];
            $nama_paket = $data_h['nama_paket'];
            $harga_paket = $data_h['harga'];
            $jumlah_produk = $data_h['jumlah_produk'];
            $tanggal_pinjaman = $data_h['tanggal_pinjam'];
            $tanggal_kembali = $data_h['tanggal_kembali'];
            $lama = $data_h['lama'];
            $nama_jasa = $data_h['nama_jasa'];
            $harga_jasa = $data_h['harga_jasa'];
            $nama_cabang = $data_h['nama_cabang'];
            $status = $data_h['status_transaksi'];
            $waktu = $data_h['waktu'];
            $total_harga=$data_h['total_harga'];

            $tanggal_akhir = new DateTime($tanggal_kembali); 
            $tanggal_now = new DateTime();
            $terlambat = $tanggal_now->diff($tanggal_akhir);
            $lambat=$terlambat->format('%d');
            (int)$denda = $lambat * 20000;
		}	
	}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Detail Transaksi</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="index.php?include=Transaksi">Transaksi</a>
            </li>
            <li class="breadcrumb-item active">Detail Transaksi</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <div class="row">
          <!-- column -->
          <div class="col-sm-12">
              <div class="card">
                  <div class="row">
                      <!-- column -->
                      <div class="col-sm-12">
                          <div class="card-body">
                              <section class="content">
                                  <div class="card card-info">
                                      <div class="card-header">
                                          <h3 class="card-title" style="margin-top:5px;">
                                                <i class="far fa-eye"></i>Form Detail Transaksi
                                          </h3>
                                      </div>
                                      <!-- /.card-header -->
                                      <!-- form start -->
                                      <br>
                                      <div class="card-body">
                                        <table class="table table-bordered">
                                            <tbody>  
                                                <tr>
                                                    <td colspan="2">
                                                        <i class="fas fa-user-circle"></i> 
                                                        <strong>Detail Transaksi</strong>
                                                    </td>
                                                </tr>               
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Nama Pelanggan</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $nama_pelanggan;?>
                                                    </td>
                                                </tr>                 
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Jenis Paket</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $nama_paket;?>
                                                    </td>
                                                </tr>                 
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Jumlah Produk</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $jumlah_produk;?>
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td>
                                                        <strong>Tanggal Pinjaman<strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $tanggal_pinjaman;?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Lama Pinjaman</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $lama;?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Tanggal Kembali</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $tanggal_kembali;?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Jenis Jasa</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $nama_jasa;?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Cabang</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $nama_cabang;?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Total Harga</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php 
                                                        $sql = "update `transaksi` set `total_harga`='$total_harga' where `id_transaksi`='$id_transaksi'";
                                                        mysqli_query($koneksi,$sql);
                                                        echo $total_harga;?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Terlambat</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php 
                                                        echo $lambat.' Hari';
                                                        
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Denda</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $denda;?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Status</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php 
                                                        if($lambat==0){
                                                            $sql = "update `transaksi` set `status_transaksi`='Dipinjam' where `id_transaksi`='$id_transaksi'";
                                                            mysqli_query($koneksi,$sql);
                                                            echo "Dipinjam";
                                                        }
                                                        if($lambat>=1){
                                                            $sql = "update `transaksi` set `status_transaksi`='Terlambat' where `id_transaksi`='$id_transaksi'";
                                                            mysqli_query($koneksi,$sql);
                                                            echo "Terlambat";
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer clearfix">
                                        <div class="text-right upgrade-btn">
                                            <a href="index.php?include=Transaksi" class="btn btn-sm btn-info d-none d-md-inline-block text-white">
                                                <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                Kembali
                                            </a>
                                        </div>
                                        &nbsp;
                                    </div>
                                  </div>
                                  <!-- /.card -->
                              </section>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- /.content -->
</div>

