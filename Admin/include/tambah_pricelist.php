<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Tambah Daftar Paket</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="index.php?include=Pricelist">Paket</a>
            </li>
            <li class="breadcrumb-item active">Tambah Daftar Paket</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <div class="row">
          <!-- column -->
          <div class="col-sm-12">
              <div class="card">
                  <div class="row">
                      <!-- column -->
                      <div class="col-sm-12">
                          <div class="card-body">
                              <section class="content">
                                  <div class="card card-info">
                                      <div class="card-header">
                                          <h3 class="card-title" style="margin-top:5px;">
                                              <i class="far fa-list-alt"></i> Form Tambah Daftar Paket
                                          </h3>
                                      </div>
                                      <!-- /.card-header -->
                                      <!-- form start -->
                                      <br>
                                      <div class="col-sm-10">
                                          <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                                              <?php if($_GET['notif']=="tambahkosong"){?>
                                                  <div class="alert alert-danger" role="alert">Maaf data
                                                      <?php echo $_GET['jenis'];?> wajib di isi
                                                  </div>
                                              <?php }?>
                                          <?php }?>
                                      </div>
                                      <form class="form-horizontal" enctype="multipart/form-data" method="post" action="index.php?include=konfirmasi_tambah_pricelist">
                                          <div class="card-body">
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Nama Paket</label>
                                                    <div class="col-sm-7">
                                                        <input type="text" name="nama_paket" class="form-control" id="nama_paket" value="<?php if(!empty($_SESSION['nama_paket'])){ echo $_SESSION['nama_paket'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Merek</label>
                                                    <div class="col-sm-7">
                                                        <select class="form-control" id="merek" name="merek">
                                                            <option value="0">- Pilih Merek -</option>
                                                            <?php
                                                            $sql_j = "select `id_merek`, `nama_merek` from `merek` order by `nama_merek";
                                                            $query_j = mysqli_query($koneksi,$sql_j);
                                                                    while($data_j = mysqli_fetch_row($query_j)){
                                                                        $id_merek = $data_j[0];
                                                                        $nama_merek = $data_j[1];
                                                            ?>
                                                            <option value="<?php echo $id_merek;?>"<?php if(!empty($_SESSION['nama_merek'])){if($id_merek==$_SESSION['nama_merek']){?> selected="selected" <?php }}?>>
                                                                <?php echo $nama_merek;?>
                                                                <?php }?>
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Type</label>
                                                    <div class="col-sm-7">
                                                        <select class="form-control" id="type" name="type">
                                                            <option value="0">- Pilih Type -</option>
                                                            <?php
                                                            $sql_j = "select `id_produk`, `type` from `produk` order by `type";
                                                            $query_j = mysqli_query($koneksi,$sql_j);
                                                                    while($data_j = mysqli_fetch_row($query_j)){
                                                                        $id_produk = $data_j[0];
                                                                        $type = $data_j[1];
                                                            ?>
                                                            <option value="<?php echo $id_produk;?>"<?php if(!empty($_SESSION['type'])){if($id_produk==$_SESSION['type']){?> selected="selected" <?php }}?>>
                                                                <?php echo $type;?>
                                                                <?php }?>
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Waktu/Jam</label>
                                                    <div class="col-sm-7">
                                                        <input type="number" name="waktu" class="form-control" id="waktu" value="<?php if(!empty($_SESSION['waktu'])){ echo $_SESSION['waktu'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Harga</label>
                                                    <div class="col-sm-7">
                                                        <input type="number" name="harga" class="form-control" id="harga" value="<?php if(!empty($_SESSION['harga'])){ echo $_SESSION['harga'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Jumlah Produk</label>
                                                    <div class="col-sm-7">
                                                        <input type="number" name="jumlah_produk" class="form-control" id="jumlah_produk" value="<?php if(!empty($_SESSION['jumlah_produk'])){ echo $_SESSION['jumlah_produk'];} ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Jenis Paket</label>
                                                    <div class="col-sm-7">
                                                        <select class="form-control" id="jenis_paket" name="jenis_paket">
                                                            <option value="0">- Pilih Jenis Paket -</option>
                                                            <option value="Weekend">Weekend</option>
                                                            <option value="Weekday">Weekday</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hobi" class="col-sm-3 col-form-label">Status</label>
                                                    <div class="col-sm-7">
                                                        <select class="form-control" id="status" name="status">
                                                            <option value="0">- Pilih Status -</option>
                                                            <option value="Perempuan">Tersedia</option>
                                                            <option value="Laki Laki">Tidak Tersedia</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                          </div>
                                          <!-- /.card-body -->
                                          <div class="card-footer">
                                              <button type="submit" class="btn btn-info float-right">
                                                  <i class="mr-3 fas fa-plus" aria-hidden="true"></i>Tambah
                                              </button>
                                              <div class="text-left upgrade-btn">
                                                  <a href="index.php?include=Produk" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                                                      <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                      Kembali
                                                  </a>
                                              </div>
                                          </div>
                                          <!-- /.card-footer -->
                                      </form>
                                  </div>
                                  <!-- /.card -->
                              </section>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- /.content -->
</div>