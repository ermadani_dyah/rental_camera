<!DOCTYPE html>
<?php 
    if((isset($_GET['aksi']))&&(isset($_GET['data']))){
		if($_GET['aksi']=='hapus'){
			$id_transaksi = $_GET['data'];
			//hapus cabang
			$sql_dh = "delete from `transaksi` where `id_transaksi` = '$id_transaksi'";
		    mysqli_query($koneksi,$sql_dh);
		}
	}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0">Transaksi</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                <a href="index.php">Home</a>
                </li>
                <li class="breadcrumb-item active">Transaksi</li>
            </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card-body">
                                <div class="card-header">
                                    <h3 class="card-title" style="margin-top:5px;">
                                        <i class="mr-3 fas fa-money-check"></i>Daftar Transaksi
                                    </h3>
                                </div> 
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <form method="post" action="index.php?include=Transaksi">
                                            <div class="row">
                                                <div class="col-md-4 bottom-10">
                                                    <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Nama Pelanggan">
                                                </div>
                                                <div class="col-md-5 bottom-10">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="mr-3 fas fa-search"aria-hidden="true"></i>&nbsp; Search
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- .row -->
                                        </form>
                                    </div>
                                    <br>
                                    <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                                <th width="5%">NO</th>
                                                <th width="15%">Nama Pelanggan</th>
                                                <th width="15%">Tanggal Pinjam</th>
                                                <th width="15%">Tanggal Kembali</th>
                                                <th width="15%">Status</th>
                                                <th width="20%">
                                                    <center>Aksi</center>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                //menampilkan data hobi
                                                $batas = 3;
                                                if(!isset($_GET['halaman'])){
                                                    $posisi = 0;
                                                    $halaman = 1;	
                                                }else{
                                                    $halaman = $_GET['halaman'];
                                                    $posisi = ($halaman-1) * $batas;
                                                }
                                                $x = $posisi+1;
                                                
                                                $sql_h = "select `T`.`id_transaksi`,`P`.`nama`,`Pa`.`nama_paket`,`Pa`.`harga`,`T`.`jumlah_produk`,
                                                `T`.`tanggal_pinjam`,`T`.`tanggal_kembali`,`T`.`lama`,`J`.`nama_jasa`,`J`.`harga_jasa`,`C`.`nama_cabang`,
                                                `T`.`total_harga`,`T`.`terlambat`,`T`.`denda`,`T`.`status_transaksi` from `transaksi` `T` inner join `pelanggan` `P`
                                                ON `T`.`id_pelanggan`=`P`.`id_pelanggan` inner join `Paket` `Pa` ON `T`.`id_paket`=`Pa`.`id_paket` inner join
                                                `jasa` `J` ON `T`.`id_jasa`=`J`.`id_jasa` inner join `cabang` `C` ON `T`.`id_cabang`=`C`.`id_cabang`"; 
                                                if (isset($_POST["katakunci"])){
                                                    $katakunci_transaksi = $_POST["katakunci"];
                                                    $_SESSION['katakunci_transaksi'] = $katakunci_transaksi;
                                                    $sql_h .= "where `P`.`nama` LIKE '%$katakunci_transaksi%'";
                                                } 
                                                $sql_h .= "order by `tanggal_kembali` limit $posisi, $batas";
                                                $query_h = mysqli_query($koneksi,$sql_h);
                                                $no=$posisi+1;
                                                while($data_h = mysqli_fetch_array($query_h)){
                                                    $id_transaksi = $data_h['id_transaksi'];
                                                    $nama_pelanggan = $data_h['nama'];
                                                    $nama_paket = $data_h['nama_paket'];
                                                    $harga_paket = $data_h['harga'];
                                                    $jumlah_produk = $data_h['jumlah_produk'];
                                                    $tanggal_pinjaman = $data_h['tanggal_pinjam'];
                                                    $tanggal_kembali = $data_h['tanggal_kembali'];
                                                    $lama = $data_h['lama'];
                                                    $nama_jasa = $data_h['nama_jasa'];
                                                    $harga_jasa = $data_h['harga'];
                                                    $nama_cabang = $data_h['nama_cabang'];
                                                    $terlambat = $data_h['terlambat'];
                                                    $denda = $data_h['denda'];
                                                    $status = $data_h['status_transaksi'];
                                                    $tanggal_akhir = new DateTime($tanggal_kembali); 
                                                    $tanggal_now = new DateTime();
                                                    $terlambat = $tanggal_now->diff($tanggal_akhir);
                                                    $lambat=$terlambat->format('%d');
                                                    
                                                     
                                            ?>	
                                            <tr>
                                                <td><?php echo $no;?></td>
                                                <td><?php echo $nama_pelanggan;?></td>
                                                <td><?php echo $tanggal_pinjaman;?></td>
                                                <td><?php echo $tanggal_kembali;?></td>
                                                <td>
                                                <?php 
                                                    if($lambat==0){
                                                        $sql = "update `transaksi` set `status_transaksi`='Dipinjam' where `id_transaksi`='$id_transaksi'";
                                                        mysqli_query($koneksi,$sql);
                                                        echo "Dipinjam";
                                                    }
                                                    if($lambat>=1){
                                                        $sql = "update `transaksi` set `status_transaksi`='Terlambat' where `id_transaksi`='$id_transaksi'";
                                                        mysqli_query($koneksi,$sql);
                                                        echo "Terlambat";
                                                    }
                                                ?>
                                                </td>
                                                <td align="center"> 
                                                    <a href="index.php?include=detail_transaksi&data=<?php echo $id_transaksi;?>" class="btn btn-xs bg-success" title="Detail">
                                                        <i class="fas fa-eye"></i>  Detail 
                                                    </a>
                                                    <a href="javascript:if(confirm('Anda yakin ingin menghapus data <?php echo $nama_pelanggan; ?>?'))window.location.href = 'index.php?include=Transaksi&aksi=hapus&data=<?php echo $id_transaksi;?>'" class="btn btn-xs btn-warning">
                                                        <i class="fas fa-trash"></i> Hapus 
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                                $no++;
                                            }?>
                                            <?php 
                                                //hitung jumlah semua data 
                                                $sql_jum = "select `T`.`id_transaksi`,`P`.`nama`,`Pa`.`nama_paket`,`T`.`jumlah_produk`,
                                                `T`.`tanggal_pinjam`,`T`.`tanggal_kembali`,`T`.`lama`,`J`.`nama_jasa`,`C`.`nama_cabang`,
                                                `T`.`total_harga`,`T`.`terlambat`,`T`.`denda`,`T`.`status_transaksi` from `transaksi` `T` inner join `pelanggan` `P`
                                                ON `T`.`id_pelanggan`=`P`.`id_pelanggan` inner join `Paket` `Pa` ON `T`.`id_paket`=`Pa`.`id_paket` inner join
                                                `jasa` `J` ON `T`.`id_jasa`=`J`.`id_jasa` inner join `cabang` `C` ON `T`.`id_cabang`=`C`.`id_cabang` "; 
                                                if (isset($_SESSION['katakunci_transaksi'])){
                                                    $katakunci_transaksi = $_SESSION['katakunci_transaksi'];
                                                    $sql_jum .= " where `P`.`nama` LIKE '%$katakunci_transaksi%' ";
                                                } 
                                                $sql_jum .= " order by `tanggal_kembali`";
                                                $query_jum = mysqli_query($koneksi,$sql_jum);
                                                $jum_data = mysqli_num_rows($query_jum);
                                                $jum_halaman = ceil($jum_data/$batas);
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <span>Showing <?php if($jum_data>0){echo $x;}else {echo 0;}?> to <?php echo $no-1;?> of <?php echo $jum_data;?> entries</span>
                                    <ul class="pagination pagination-sm m-0 float-right">
                                        <?php 
                                            if($jum_halaman==0){
                                                //tidak ada halaman
                                            }else if($jum_halaman==1){
                                                echo "<li class='page-item'><a class='page-link'>1</a></li>";
                                            }else{
                                                $sebelum = $halaman-1;
                                                $setelah = $halaman+1;                  
                                                if($halaman!=1){
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Transaksi&halaman=1'>First</a></li>";
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Transaksi&halaman=$sebelum'>«</a></li>";
                                                }
                                                //menampilkan angka halaman
                                                for($i=1; $i<=$jum_halaman; $i++){
                                                    if($i!=$halaman){
                                                        echo "<li class='page-item'><a class='page-link' href='index.php?include=Transaksi&halaman=$i'>$i</a></li>";
                                                    }else{
                                                        echo "<li class='page-item'><a class='page-link'>$i</a></li>";
                                                    }
                                                }
                                                if($halaman!=$jum_halaman){
                                                    echo "<li class='page-item'><a class='page-link'  href='index.php?include=Transaksi&halaman=$setelah'>»</a></li>";
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Transaksi&halaman=$jum_halaman'>Last</a></li>";
                                                }
                                            }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
</div>
