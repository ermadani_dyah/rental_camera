<?php 
	if(isset($_GET['data'])){
		$id_produk = $_GET['data'];
		//get data produk
		$sql_m = "select `p`.`id_produk`, `p`.`nama_produk`, `m`.`nama_merek`,`p`.`type`,`p`.`gambar`,`p`.`spesifikasi`,
        `p`.`kelengkapan` from `produk` `p`inner join `merek` `m` on `p`.`id_merek` = `m`.`id_merek` WHERE `p`.`id_produk`='$id_produk'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$id_produk = $data_m[0];
            $nama_produk = $data_m[1];
            $merek = $data_m[2];
            $type = $data_m[3];
            $gambar= $data_m[4];
            $spesifikasi = $data_m[5];
            $kelengkapan= $data_m[6];
		}	
	}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Detail Produk</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="index.php?include=Produk">Produk</a>
            </li>
            <li class="breadcrumb-item active">Detail Produk</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <div class="row">
          <!-- column -->
          <div class="col-sm-12">
              <div class="card">
                  <div class="row">
                      <!-- column -->
                      <div class="col-sm-12">
                          <div class="card-body">
                              <section class="content">
                                  <div class="card card-info">
                                      <div class="card-header">
                                          <h3 class="card-title" style="margin-top:5px;">
                                                <i class="far fa-eye"></i>Form Detail Produk
                                          </h3>
                                      </div>
                                      <!-- /.card-header -->
                                      <!-- form start -->
                                      <br>
                                      <div class="card-body">
                                        <table class="table table-bordered">
                                            <tbody>  
                                                <tr>
                                                    <td colspan="2">
                                                        <i class="fas fa-user-circle"></i> 
                                                        <strong>Detail Produk</strong>
                                                    </td>
                                                </tr>               
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Nama Produk</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $nama_produk;?>
                                                    </td>
                                                </tr>                 
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Merek Produk</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $merek;?>
                                                    </td>
                                                </tr>                 
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Type Produk</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $type;?>
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td>
                                                        <strong>Gambar Produk<strong>
                                                    </td>
                                                    <td>
                                                        <img src="dist/img/<?php echo $gambar;?>" class="img-fluid" width="200px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Spesifikasi</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $spesifikasi;?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Kelengkapan</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $kelengkapan;?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer clearfix">
                                        <div class="text-right upgrade-btn">
                                            <a href="index.php?include=Produk" class="btn btn-sm btn-info d-none d-md-inline-block text-white">
                                                <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                Kembali
                                            </a>
                                        </div>
                                        &nbsp;
                                    </div>
                                  </div>
                                  <!-- /.card -->
                              </section>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- /.content -->
</div>
