<?php 
	if(isset($_GET['data'])){
		$id_pelanggan = $_GET['data'];
		//get data produk
		$sql_m = "select * from `pelanggan` where `id_pelanggan` = '$id_pelanggan'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$id_pelanggan = $data_m[0];
            $nama_pelanggan = $data_m[1];
            $alamat = $data_m[2];
            $hp = $data_m[3];
            $email = $data_m[4];
            $identitas = $data_m[5];
            $jenis_kelamin= $data_m[6];
            $username= $data_m[7];
		}	
	}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Detail Pelanggan</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="index.php?include=Pelanggan">Pelanggan</a>
            </li>
            <li class="breadcrumb-item active">Detail Pelanggan</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <div class="row">
          <!-- column -->
          <div class="col-sm-12">
              <div class="card">
                  <div class="row">
                      <!-- column -->
                      <div class="col-sm-12">
                          <div class="card-body">
                              <section class="content">
                                  <div class="card card-info">
                                      <div class="card-header">
                                          <h3 class="card-title" style="margin-top:5px;">
                                                <i class="far fa-eye"></i>Form Detail Pelanggan
                                          </h3>
                                      </div>
                                      <!-- /.card-header -->
                                      <!-- form start -->
                                      <br>
                                      <div class="card-body">
                                        <table class="table table-bordered">
                                            <tbody>  
                                                <tr>
                                                    <td colspan="2">
                                                        <i class="fas fa-user-circle"></i> 
                                                        <strong>Detail Pelanggan</strong>
                                                    </td>
                                                </tr>               
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Nama Pelanggan</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $nama_pelanggan;?>
                                                    </td>
                                                </tr>                 
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Alamat</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $alamat;?>
                                                    </td>
                                                </tr>                 
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Nomer Hp</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $hp;?>
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Email</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $email;?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Nomer Identitas KTP</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $identitas;?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <strong>Jenis Kelamin</strong>
                                                    </td>
                                                    <td width="80%">
                                                        <?php echo $jenis_kelamin;?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer clearfix">
                                        <div class="text-right upgrade-btn">
                                            <a href="index.php?include=Pelanggan" class="btn btn-sm btn-info d-none d-md-inline-block text-white">
                                                <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                                                Kembali
                                            </a>
                                        </div>
                                        &nbsp;
                                    </div>
                                  </div>
                                  <!-- /.card -->
                              </section>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- /.content -->
</div>
