<!DOCTYPE html>
<?php 
    if((isset($_GET['aksi']))&&(isset($_GET['data']))){
		if($_GET['aksi']=='hapus'){
			$id_pelanggan = $_GET['data'];
			//hapus cabang
			$sql_dh = "delete from `pelanggan` where `id_pelanggan` = '$id_pelanggan'";
		    mysqli_query($koneksi,$sql_dh);
		}
	}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0">Pelanggan</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                <a href="index.php">Home</a>
                </li>
                <li class="breadcrumb-item active">Pelanggan</li>
            </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card-body">
                                <div class="card-header">
                                    <h3 class="card-title" style="margin-top:5px;">
                                        <i class="mr-3 fas fa-user"></i> Daftar Pelanggan
                                    </h3>
                                </div> 
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <form method="post" action="index.php?include=Pelanggan">
                                            <div class="row">
                                                <div class="col-md-4 bottom-10">
                                                    <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Nama pelanggan & Identitas">
                                                </div>
                                                <div class="col-md-5 bottom-10">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="mr-3 fas fa-search"aria-hidden="true"></i>&nbsp; Search
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- .row -->
                                        </form>
                                    </div>
                                    <br>
                                    <div class="col-sm-12">
                                        <?php if(!empty($_GET['notif'])){?>
                                            <?php if($_GET['notif']=="tambahberhasil"){?>
                                                <div class="alert alert-success" role="alert">
                                                    Data Berhasil Ditambahkan
                                                </div>
                                            <?php } else if($_GET['notif']=="editberhasil"){?>
                                                <div class="alert alert-success" role="alert">
                                                    Data Berhasil Diubah
                                                </div>
                                            <?php }?>
                                        <?php }?>
                                    </div>
                                    <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                                <th width="5%">NO</th>
                                                <th width="15%">Nama</th>
                                                <th width="15%">No Hp</th>
                                                <th width="15%">Email</th>
                                                <th width="15%">Identitas KTP</th>
                                                <th width="20%">
                                                    <center>Aksi</center>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                //menampilkan data hobi
                                                $batas = 3;
                                                if(!isset($_GET['halaman'])){
                                                    $posisi = 0;
                                                    $halaman = 1;	
                                                }else{
                                                    $halaman = $_GET['halaman'];
                                                    $posisi = ($halaman-1) * $batas;
                                                }
                                                $x = $posisi+1;
                                                $sql_h = "select * from `pelanggan`"; 
                                                if (isset($_POST["katakunci"])){
                                                    $katakunci_pelanggan = $_POST["katakunci"];
                                                    $_SESSION['katakunci_cabang'] = $katakunci_pelanggan;
                                                    $sql_h .= " where `nama` LIKE '%$katakunci_pelanggan%' OR `identitas` LIKE '%$katakunci_pelanggan%' ";
                                                } 
                                                $sql_h .= "order by `nama` limit $posisi, $batas";
                                                $query_h = mysqli_query($koneksi,$sql_h);
                                                $no=$posisi+1;
                                                while($data_h = mysqli_fetch_row($query_h)){
                                                    $id_pelanggan = $data_h[0];
                                                    $nama_pelanggan = $data_h[1];
                                                    $alamat = $data_h[2];
                                                    $hp = $data_h[3];
                                                    $email = $data_h[4];
                                                    $identitas = $data_h[5];
                                                    $jenis_kelamin= $data_h[6];
                                                    $username= $data_h[7];
                                            ?>	
                                            <tr>
                                                <td><?php echo $no;?></td>
                                                <td><?php echo $nama_pelanggan;?></td>
                                                <td><?php echo $hp;?></td>
                                                <td><?php echo $email;?></td>
                                                <td><?php echo $identitas;?></td>
                                                <td align="center">
                                                    <a href="index.php?include=edit_pelanggan&data=<?php echo $id_pelanggan;?>" class="btn btn-xs btn-info">
                                                        <i class="fas fa-edit"></i> 
                                                    </a>
                                                    <a href="javascript:if(confirm('Anda yakin ingin menghapus data <?php echo $nama_pelanggan; ?>?'))window.location.href = 'index.php?include=Pelanggan&aksi=hapus&data=<?php echo $id_pelanggan;?>'" class="btn btn-xs btn-warning">
                                                        <i class="fas fa-trash"></i> 
                                                    </a>
                                                    <a href="index.php?include=detail_pelanggan&data=<?php echo $id_pelanggan;?>" class="btn btn-xs bg-success" title="Detail">
                                                        <i class="fas fa-eye"></i> 
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                                $no++;
                                            }?>
                                            <?php 
                                                //hitung jumlah semua data 
                                                $sql_jum = "select * from `pelanggan`"; 
                                                if (isset($_SESSION['katakunci_pelanggan'])){
                                                    $katakunci_pelanggan = $_SESSION['katakunci_pelanggan'];
                                                    $sql_jum .= " where `nama` LIKE '%$katakunci_pelanggan%' OR `identitas` LIKE '%$katakunci_pelanggan%'";
                                                } 
                                                $sql_jum .= " order by `nama`";
                                                $query_jum = mysqli_query($koneksi,$sql_jum);
                                                $jum_data = mysqli_num_rows($query_jum);
                                                $jum_halaman = ceil($jum_data/$batas);
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <span>Showing <?php if($jum_data>0){echo $x;}else {echo 0;}?> to <?php echo $no-1;?> of <?php echo $jum_data;?> entries</span>
                                    <ul class="pagination pagination-sm m-0 float-right">
                                        <?php 
                                            if($jum_halaman==0){
                                                //tidak ada halaman
                                            }else if($jum_halaman==1){
                                                echo "<li class='page-item'><a class='page-link'>1</a></li>";
                                            }else{
                                                $sebelum = $halaman-1;
                                                $setelah = $halaman+1;                  
                                                if($halaman!=1){
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Pelanggan&halaman=1'>First</a></li>";
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Pelanggan&halaman=$sebelum'>«</a></li>";
                                                }
                                                //menampilkan angka halaman
                                                for($i=1; $i<=$jum_halaman; $i++){
                                                    if($i!=$halaman){
                                                        echo "<li class='page-item'><a class='page-link' href='index.php?include=Pelanggan&halaman=$i'>$i</a></li>";
                                                    }else{
                                                        echo "<li class='page-item'><a class='page-link'>$i</a></li>";
                                                    }
                                                }
                                                if($halaman!=$jum_halaman){
                                                    echo "<li class='page-item'><a class='page-link'  href='index.php?include=Pelanggan&halaman=$setelah'>»</a></li>";
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Pelanggan&halaman=$jum_halaman'>Last</a></li>";
                                                }
                                            }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
</div>
