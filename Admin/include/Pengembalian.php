<!DOCTYPE html>
<?php 
    if((isset($_GET['aksi']))&&(isset($_GET['data']))){
		if($_GET['aksi']=='hapus'){
			$id_pengembalian = $_GET['data'];
			//hapus cabang
			$sql_dh = "delete from `pengembalian` where `id_pengembalian` = '$id_pengembalian'";
		    mysqli_query($koneksi,$sql_dh);
		}
	}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0">Pengembalian</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item">
                <a href="index.php">Home</a>
                </li>
                <li class="breadcrumb-item active">Pengembalian</li>
            </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- column -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="row">
                        <!-- column -->
                        <div class="col-sm-12">
                            <div class="card-body">
                                <div class="card-header">
                                    <h3 class="card-title" style="margin-top:5px;">
                                        <i class="mr-3 fas fa-money-check"></i>Daftar Pengembalian
                                    </h3>
                                </div> 
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <form method="post" action="index.php?include=Pengembalian">
                                            <div class="row">
                                                <div class="col-md-4 bottom-10">
                                                    <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Nama Transaksi">
                                                </div>
                                                <div class="col-md-5 bottom-10">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="mr-3 fas fa-search"aria-hidden="true"></i>&nbsp; Search
                                                    </button>
                                                </div>
                                            </div>
                                            <!-- .row -->
                                        </form>
                                    </div>
                                    <br>
                                    <table class="table table-bordered">
                                        <thead>                  
                                            <tr>
                                                <th width="5%">NO</th>
                                                <th width="15%">Nomor Transaksi</th>
                                                <th width="15%">Tanggal Kembali</th>
                                                <th width="15%">Status</th>
                                                <th width="20%">
                                                    <center>Aksi</center>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                //menampilkan data hobi
                                                $batas = 3;
                                                if(!isset($_GET['halaman'])){
                                                    $posisi = 0;
                                                    $halaman = 1;	
                                                }else{
                                                    $halaman = $_GET['halaman'];
                                                    $posisi = ($halaman-1) * $batas;
                                                }
                                                $x = $posisi+1;
                                                
                                                $sql_h = "select `P`.`id_pengembalian`,`T`.`id_transaksi`,`P`.`tanggal_kembali`,`P`.`status` FROM `pengembalian` `P` inner join `transaksi` `T` ON `P`.`id_transaksi`=`T`.`id_transaksi`"; 
                                                if (isset($_POST["katakunci"])){
                                                    $katakunci_pengembalian = $_POST["katakunci"];
                                                    $_SESSION['katakunci_pengembalian'] = $katakunci_pengembalian;
                                                    $sql_h .= "where `T`.`id_transaksi` LIKE '%$katakunci_pengembalian%'";
                                                } 
                                                $sql_h .= "order by `status` limit $posisi, $batas";
                                                $query_h = mysqli_query($koneksi,$sql_h);
                                                $no=$posisi+1;
                                                while($data_h = mysqli_fetch_array($query_h)){
                                                    $id_pengembalian = $data_h['id_pengembalian'];
                                                    $id_transaksi = $data_h['id_transaksi'];
                                                    $tanggal_kembali=$data_h['tanggal_kembali'];
                                                    $status = $data_h['status']; 
                                            ?>	
                                            <tr>
                                                <td><?php echo $no;?></td>
                                                <td><?php echo $id_transaksi;?></td>
                                                <td><?php echo $tanggal_kembali;?></td>
                                                <td><?php echo $status;?></td>
                                                <td align="center"> 
                                                    <a href="javascript:if(confirm('Anda yakin ingin menghapus data <?php echo $id_transaksi; ?>?'))window.location.href = 'index.php?include=Pengembalian&aksi=hapus&data=<?php echo $id_pengembalian;?>'" class="btn btn-xs btn-warning">
                                                        <i class="fas fa-trash"></i> Hapus 
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                                $no++;
                                            }?>
                                            <?php 
                                                //hitung jumlah semua data 
                                                $sql_jum = "select `P`.`id_pengembalian`,`T`.`id_transaksi`,`P`.`tanggal_kembali`,`P`.`status` FROM `pengembalian` `P` inner join `transaksi` `T` ON `P`.`id_transaksi`=`T`.`id_transaksi`"; 
                                                if (isset($_SESSION['katakunci_pengembalian'])){
                                                    $katakunci_pengembalian = $_SESSION['katakunci_pengembalian'];
                                                    $sql_jum .= " where `T`.`id_transaksi` LIKE '%$katakunci_pengembalian%' ";
                                                } 
                                                $sql_jum .= " order by `status`";
                                                $query_jum = mysqli_query($koneksi,$sql_jum);
                                                $jum_data = mysqli_num_rows($query_jum);
                                                $jum_halaman = ceil($jum_data/$batas);
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <span>Showing <?php if($jum_data>0){echo $x;}else {echo 0;}?> to <?php echo $no-1;?> of <?php echo $jum_data;?> entries</span>
                                    <ul class="pagination pagination-sm m-0 float-right">
                                        <?php 
                                            if($jum_halaman==0){
                                                //tidak ada halaman
                                            }else if($jum_halaman==1){
                                                echo "<li class='page-item'><a class='page-link'>1</a></li>";
                                            }else{
                                                $sebelum = $halaman-1;
                                                $setelah = $halaman+1;                  
                                                if($halaman!=1){
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Pengembalian&halaman=1'>First</a></li>";
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Pengembalian&halaman=$sebelum'>«</a></li>";
                                                }
                                                //menampilkan angka halaman
                                                for($i=1; $i<=$jum_halaman; $i++){
                                                    if($i!=$halaman){
                                                        echo "<li class='page-item'><a class='page-link' href='index.php?include=Pengembalian&halaman=$i'>$i</a></li>";
                                                    }else{
                                                        echo "<li class='page-item'><a class='page-link'>$i</a></li>";
                                                    }
                                                }
                                                if($halaman!=$jum_halaman){
                                                    echo "<li class='page-item'><a class='page-link'  href='index.php?include=Pengembalian&halaman=$setelah'>»</a></li>";
                                                    echo "<li class='page-item'><a class='page-link' href='index.php?include=Pengembalian&halaman=$jum_halaman'>Last</a></li>";
                                                }
                                            }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
</div>
