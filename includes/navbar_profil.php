<!-- Logo & Site Name -->
<div class="placeholder">
    <div class="parallax-window" data-parallax="scroll" data-image-src="img/gambar 2.jpg">
        <div class="tm-header">
            <div class="row tm-header-inner">
                <div class="col-md-6 col-12">
                    <img src="img/simple-house-logo.png" alt="Logo" class="tm-site-logo" /> 
                    <div class="tm-site-text-box">
                        <h1 class="tm-site-title" style="color:white">Kamera Kuyy</h1>
                        <h6 class="tm-site-description" style="color:white">100% Rental Kamera Terpecaya</h6>	
                    </div>
                </div>
                <nav class="col-md-6 col-12 tm-nav">
                    <ul class="tm-nav-ul">
                        <li class="tm-nav-li">
                            <a href="index.php" class="tm-nav-link">Home</a>
                        </li>
                        <li class="tm-nav-li">
                            <a href="index.php?include=Cabang" class="tm-nav-link">Cabang</a>
                        </li>
                        <li class="tm-nav-li">
                            <a href="index.php?include=Logout" class="tm-nav-link">Logout</a>
                        </li>
                    </ul>
                </nav>	
            </div>
        </div>
    </div>
</div>