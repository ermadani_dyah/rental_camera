<!DOCTYPE html>
<?php
    if(isset($_GET['data'])){
		$id_transaksi = $_GET['data'];
		$_SESSION['id_transaksi']=$id_transaksi;
		$sql_m = "select `T`.`id_transaksi`,`P`.`nama`,`Pa`.`nama_paket`,`Pa`.`harga`,`T`.`jumlah_produk`,
        `T`.`tanggal_pinjam`,`T`.`tanggal_kembali`,`T`.`lama`,`J`.`nama_jasa`,`J`.`harga_jasa`,`C`.`nama_cabang`,
        `T`.`total_harga`,`T`.`terlambat`,`T`.`denda`,`T`.`status_transaksi` ,`Pa`.`waktu` from `transaksi` `T` inner join `pelanggan` `P`
        ON `T`.`id_pelanggan`=`P`.`id_pelanggan` inner join `Paket` `Pa` ON `T`.`id_paket`=`Pa`.`id_paket` inner join
        `jasa` `J` ON `T`.`id_jasa`=`J`.`id_jasa` inner join `cabang` `C` ON `T`.`id_cabang`=`C`.`id_cabang` WHERE `T`.`id_transaksi` ='$id_transaksi'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_h = mysqli_fetch_array($query_m)){
			$id_transaksi = $data_h['id_transaksi'];
            $nama_pelanggan = $data_h['nama'];
            $nama_paket = $data_h['nama_paket'];
            $harga_paket = $data_h['harga'];
            $jumlah_produk = $data_h['jumlah_produk'];
            $tanggal_pinjaman = $data_h['tanggal_pinjam'];
            $tanggal_kembali = $data_h['tanggal_kembali'];
            $lama = $data_h['lama'];
            $nama_jasa = $data_h['nama_jasa'];
            $harga_jasa = $data_h['harga_jasa'];
            $nama_cabang = $data_h['nama_cabang'];
            $status = $data_h['status_transaksi'];
            $waktu = $data_h['waktu'];
            $total_harga=$data_h['total_harga'];

		} 
	}
?>
<header class="row tm-welcome-section">
    <h2 class="col-12 text-center tm-section-title">Terimakasih</h2>
    <p class="col-12 text-center">Terimakasih Sudah Melakukan Transaksi</p>
</header>
<div class="tm-container-inner tm-persons">
    <div class="mb-5"> 
        <table class="table table-bordered" style="color:black; border: 1px solid black; margin-left:auto;margin-right:auto" >
            <tbody style="text-align:center;">  
                <tr>
                    <td colspan="2">
                        <i class="fas fa-check"></i>
                        <strong>Status Berhasil</strong>
                        <br>
                        Terimakasih Sudah melakukan transaksi dan penyewaan samapai jumpa lagi
                        <br>
                        <a href="index.php" class="tm-btn tm-btn-default tm-right">Kembali</a>
                    </td>
                </tr>               
            </tbody>
        </table> 
    </div>
</div>