<!DOCTYPE html>
<?php
    if(isset($_GET['data'])){
		$id_transaksi = $_GET['data'];
		$_SESSION['id_transaksi']=$id_transaksi;
		$sql_m = "select `T`.`id_transaksi`,`P`.`nama`,`P`.`jenis_kelamin`,`P`.`alamat`,
        `P`.`no_hp`,`P`.`email`,`P`.`identitas`,`P`.`username`,`Pa`.`nama_paket`,`Pa`.`harga`,`Pa`.`id_produk`,
        `T`.`jumlah_produk`,`T`.`tanggal_pinjam`,`T`.`tanggal_kembali`,`T`.`lama`,`J`.`nama_jasa`,
        `J`.`harga_jasa`,`C`.`nama_cabang`, `T`.`total_harga`,`T`.`terlambat`,`T`.`denda`,`T`.`status_transaksi` 
        ,`Pa`.`waktu` from `transaksi` `T` inner join `pelanggan` `P` ON `T`.`id_pelanggan`=`P`.`id_pelanggan` inner join 
        `Paket` `Pa` ON `T`.`id_paket`=`Pa`.`id_paket` inner join `jasa` `J` ON `T`.`id_jasa`=`J`.`id_jasa` inner join 
        `cabang` `C` ON `T`.`id_cabang`=`C`.`id_cabang` WHERE `T`.`id_transaksi` ='$id_transaksi'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_h = mysqli_fetch_array($query_m)){
			$id_transaksi = $data_h['id_transaksi'];
            $nama_pelanggan = $data_h['nama'];
            $jenis_kelamin = $data_h['jenis_kelamin'];
            $alamat = $data_h['alamat'];
            $no_hp = $data_h['no_hp'];
            $email = $data_h['email'];
            $identitas = $data_h['identitas'];
            $username = $data_h['username'];
            $nama_paket = $data_h['nama_paket'];
            $harga_paket = $data_h['harga'];
            $id_produk = $data_h['id_produk'];
            $jumlah_produk = $data_h['jumlah_produk'];
            $tanggal_pinjaman = $data_h['tanggal_pinjam'];
            $tanggal_kembali = $data_h['tanggal_kembali'];
            $lama = $data_h['lama'];
            $nama_jasa = $data_h['nama_jasa'];
            $harga_jasa = $data_h['harga_jasa'];
            $nama_cabang = $data_h['nama_cabang'];
            $status = $data_h['status_transaksi'];
            $waktu = $data_h['waktu'];
            $total_harga=$data_h['total_harga'];
            $tanggal_akhir = new DateTime($tanggal_kembali); 
            $tanggal_now = new DateTime();
            $terlambat = $tanggal_now->diff($tanggal_akhir);
            $lambat=$terlambat->format('%d');
            (int)$denda = $lambat * 20000;

		} 
        $sql_o = "select `gambar` FROM  produk where `id_produk` = '$id_produk'";
		$query_o = mysqli_query($koneksi,$sql_o);
		while($data_o = mysqli_fetch_row($query_o)){
			$gambar=$data_o[0];
		}
?>
        <header class="row tm-welcome-section">
            <h2 class="col-12 text-center tm-section-title">Profil Anda ANDA</h2>
            <p class="col-12 text-center">Informasi Transaksi Anda</p>
        </header>
        <div class="tm-container-inner tm-persons">
            <div class="row"> 
                <article class="col-lg-6">
                    <figure class="tm-person">
                        <img src="img/about-01.jpg" alt="Image" class="img-fluid tm-person-img" />
                        <figcaption class="tm-person-description">
                            <h4 class="tm-person-name"><?php echo $nama_pelanggan?></h4>
                            <p class="tm-person-title"><?php echo $username?></p>
                            <p class="tm-person-about">Alamat: <?php echo $alamat?></p>
                            <p class="tm-person-about">Jenis Kelamin: <?php echo $jenis_kelamin?></p>
                            <p class="tm-person-about"><i class="fas fa-envelope "></i> : <?php echo $email?></p>
                            <p class="tm-person-about"><i class="fab fa-whatsapp"></i> : <?php echo $no_hp?></p>
                        </figcaption>
                    </figure>
                </article>
                <article class="col-lg-6">
                    <figure class="tm-person">
                        <img src="Admin/dist/img/<?php echo $gambar?>" alt="Image" class="img-fluid tm-person-img" style="width:250px" />
                        <figcaption class="tm-person-description">
                            <h4 class="tm-person-name">Transaksi Anda</h4>
                            <p class="tm-person-about">Nama Paket: <?php echo $nama_paket?></p>
                            <p class="tm-person-about">Jumlah Produk: <?php echo $jumlah_produk?></p>
                            <p class="tm-person-about">Tanggal Pinjam: <?php echo $tanggal_pinjaman?></p>
                            <p class="tm-person-about">Tanggal Kembali: <?php echo $tanggal_kembali?></p>
                            <p class="tm-person-title">Status: <?php echo $status?></p>
                        </figcaption>
                    </figure>
                </article>
            </div>
        </div>
        <div class="tm-container-inner tm-persons">
            <div class="mb-5"> 
                <table class="table table-bordered" style="color:black; border: 1px solid black; margin-left:auto;margin-right:auto" >
                    <tbody>  
                        <tr>
                            <td colspan="2">
                                <h4 class="tm-person-name">Denda Anda</h4>
                            </td>
                        </tr>               
                        <tr>
                            <td width="20%">
                                <strong>Tanggal Pinjam:   </strong>
                            </td>
                            <td width="80%">
                                <?php echo $tanggal_pinjaman;?>
                            </td>
                        </tr>                 
                        <tr>
                            <td width="20%">
                                <strong>Tanggal Kembali:   </strong>
                            </td>
                            <td width="80%">
                                <?php echo $tanggal_kembali;?>
                            </td>
                        </tr>                 
                        <tr>
                            <td width="20%">
                                <strong>Terlambat:   </strong>
                            </td>
                            <td width="80%">
                                <?php echo $lambat;?>
                            </td>
                        </tr> 
                        <tr>
                            <td width="20%">
                                <strong>Denda:   <strong>
                            </td>
                            <td width="80%">
                                <?php echo $denda;?>
                            </td>
                        </tr>
                    </tbody>
                </table> 
                <br>
                <hr>
                <?php
                    if($lambat==0){
                ?>
                    <h3>Terimakasih Sudah Mempercayai Kami</h3>
                    <p>
                        Silahkan anda melakukan pengembalian Dibutton bawah ini
                    </p>
                    <br>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <a href="index.php?include=Pengembalian&data=<?php echo $id_transaksi?>" class="tm-btn tm-btn-default tm-right">Pengembalian</a>
                        </div>
                    </div>
                <?php
                    }else if($lambat>=1){
                ?>
                    <h3>Silahkan Lakukan Pembayaran Denda Jika Anda Terlambat</h3>
                    <p>
                        Silahkan anda membayar tagihan anda dengan cara transfer via Bank BRI di nomor Rekening : <br>
                        <strong>(0986-01-025805-53-8 a/n SEWA MOBIL)</strong> untuk menyelesaikan pembayaran. dan untuk uang pembayaran denda
                    </p>
                    <p>Status akan berubah dalam 1x24 jam</p>
                    <br>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <a href="index.php?include=Pengembalian&data=<?php echo $id_transaksi?>" class="tm-btn tm-btn-default tm-right">Pengembalian</a>
                        </div>
                    </div>
                <?php
                    }
                ?>
            </div>
        </div>
<?php 
    }else if(isset($_GET['pelanggan'])){
        $id_pelanggan = $_GET['pelanggan'];
		$_SESSION['id_pelanggan']=$id_pelanggan;
        $sql_m = "select `T`.`id_transaksi`,`P`.`nama`,`P`.`jenis_kelamin`,`P`.`alamat`,
        `P`.`no_hp`,`P`.`email`,`P`.`identitas`,`P`.`username`,`Pa`.`nama_paket`,`Pa`.`harga`,`Pa`.`id_produk`,
        `T`.`jumlah_produk`,`T`.`tanggal_pinjam`,`T`.`tanggal_kembali`,`T`.`lama`,`J`.`nama_jasa`,
        `J`.`harga_jasa`,`C`.`nama_cabang`, `T`.`total_harga`,`T`.`terlambat`,`T`.`denda`,`T`.`status_transaksi` 
        ,`Pa`.`waktu` from `transaksi` `T` inner join `pelanggan` `P` ON `T`.`id_pelanggan`=`P`.`id_pelanggan` inner join 
        `Paket` `Pa` ON `T`.`id_paket`=`Pa`.`id_paket` inner join `jasa` `J` ON `T`.`id_jasa`=`J`.`id_jasa` inner join 
        `cabang` `C` ON `T`.`id_cabang`=`C`.`id_cabang` WHERE `P`.`id_pelanggan` ='$id_pelanggan'";
        $query_m = mysqli_query($koneksi,$sql_m);
        $transaksi=null;
		while($data_h = mysqli_fetch_array($query_m)){
			$id_transaksi = $data_h['id_transaksi'];
            $nama_pelanggan = $data_h['nama'];
            $jenis_kelamin = $data_h['jenis_kelamin'];
            $alamat = $data_h['alamat'];
            $no_hp = $data_h['no_hp'];
            $email = $data_h['email'];
            $identitas = $data_h['identitas'];
            $username = $data_h['username'];
            $nama_paket = $data_h['nama_paket'];
            $harga_paket = $data_h['harga'];
            $id_produk = $data_h['id_produk'];
            $jumlah_produk = $data_h['jumlah_produk'];
            $tanggal_pinjaman = $data_h['tanggal_pinjam'];
            $tanggal_kembali = $data_h['tanggal_kembali'];
            $lama = $data_h['lama'];
            $nama_jasa = $data_h['nama_jasa'];
            $harga_jasa = $data_h['harga_jasa'];
            $nama_cabang = $data_h['nama_cabang'];
            $status = $data_h['status_transaksi'];
            $waktu = $data_h['waktu'];
            $total_harga=$data_h['total_harga'];
            $tanggal_akhir = new DateTime($tanggal_kembali); 
            $tanggal_now = new DateTime();
            $terlambat = $tanggal_now->diff($tanggal_akhir);
            $lambat=$terlambat->format('%d');
            (int)$denda = $lambat * 20000;

		} 
?>
        <header class="row tm-welcome-section">
            <h2 class="col-12 text-center tm-section-title">Profil Anda ANDA</h2>
            <p class="col-12 text-center">Informasi Transaksi Anda</p>
        </header>
            <div class="tm-container-inner tm-persons">
                <div class="row">
                    <?php
                        $sql_o = "select * FROM  pelanggan where `id_pelanggan` = '$id_pelanggan'";
                        $query_o = mysqli_query($koneksi,$sql_o);
                        while($data_o = mysqli_fetch_row($query_o)){
                            $id_pelanggan = $data_o[0];
                            $nama_pelanggan = $data_o[1];
                            $alamat = $data_o[2];
                            $hp = $data_o[3];
                            $email = $data_o[4];
                            $identitas = $data_o[5];
                            $jenis_kelamin= $data_o[6];
                            $username= $data_o[7];
                        }
                        
                    ?>
                    <article class="col-lg-6">
                        <figure class="tm-person">
                            <img src="img/about-01.jpg" alt="Image" class="img-fluid tm-person-img" />
                            <figcaption class="tm-person-description">
                                <h4 class="tm-person-name"><?php echo $nama_pelanggan?></h4>
                                <p class="tm-person-title"><?php echo $username?></p>
                                <p class="tm-person-about">Alamat: <?php echo $alamat?></p>
                                <p class="tm-person-about">Jenis Kelamin: <?php echo $jenis_kelamin?></p>
                                <p class="tm-person-about"><i class="fas fa-envelope "></i> : <?php echo $email?></p>
                                <p class="tm-person-about"><i class="fab fa-whatsapp"></i> : <?php echo $hp?></p>
                            </figcaption>
                        </figure>
                    </article>
                    <?php
                        if(isset($id_transaksi)){
                            $sql_o = "select `gambar` FROM  produk where `id_produk` = '$id_produk'";
                            $query_o = mysqli_query($koneksi,$sql_o);
                            while($data_o = mysqli_fetch_row($query_o)){
                                $gambar=$data_o[0];
                        }
                    ?>
                    <article class="col-lg-6">
                        <figure class="tm-person">
                            <img src="Admin/dist/img/<?php echo $gambar?>" alt="Image" class="img-fluid tm-person-img" style="width:250px" />
                            <figcaption class="tm-person-description">
                                <h4 class="tm-person-name">Transaksi Anda</h4>
                                <p class="tm-person-about">Nama Paket: <?php echo $nama_paket?></p>
                                <p class="tm-person-about">Jumlah Produk: <?php echo $jumlah_produk?></p>
                                <p class="tm-person-about">Tanggal Pinjam: <?php echo $tanggal_pinjaman?></p>
                                <p class="tm-person-about">Tanggal Kembali: <?php echo $tanggal_kembali?></p>
                                <p class="tm-person-title">Status: <?php echo $status?></p>
                            </figcaption>
                        </figure>
                    </article>
                </div>
            </div>
            <div class="tm-container-inner tm-persons">
                <div class="mb-5"> 
                    <table class="table table-bordered" style="color:black; border: 1px solid black; margin-left:auto;margin-right:auto" >
                        <tbody>  
                            <tr>
                                <td colspan="2">
                                    <h4 class="tm-person-name">Denda Anda</h4>
                                </td>
                            </tr>               
                            <tr>
                                <td width="20%">
                                    <strong>Tanggal Pinjam:   </strong>
                                </td>
                                <td width="80%">
                                    <?php echo $tanggal_pinjaman;?>
                                </td>
                            </tr>                 
                            <tr>
                                <td width="20%">
                                    <strong>Tanggal Kembali:   </strong>
                                </td>
                                <td width="80%">
                                    <?php echo $tanggal_kembali;?>
                                </td>
                            </tr>                 
                            <tr>
                                <td width="20%">
                                    <strong>Terlambat:   </strong>
                                </td>
                                <td width="80%">
                                    <?php echo $lambat;?>
                                </td>
                            </tr> 
                            <tr>
                                <td width="20%">
                                    <strong>Denda:   <strong>
                                </td>
                                <td width="80%">
                                    <?php echo $denda;?>
                                </td>
                            </tr>
                        </tbody>
                    </table> 
                    <br>
                    <hr>
                    <?php
                        if($lambat==0){
                    ?>
                        <h3>Terimakasih Sudah Mempercayai Kami</h3>
                        <p>
                            Silahkan anda melakukan pengembalian Dibutton bawah ini
                        </p>
                        <br>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <a href="index.php?include=Pengembalian&data=<?php echo $id_transaksi?>" class="tm-btn tm-btn-default tm-right">Pengembalian</a>
                            </div>
                        </div>
                    <?php
                        }else if($lambat>=1){
                    ?>
                        <h3>Silahkan Lakukan Pembayaran Denda Jika Anda Terlambat</h3>
                        <p>
                            Silahkan anda membayar tagihan anda dengan cara transfer via Bank BRI di nomor Rekening : <br>
                            <strong>(0986-01-025805-53-8 a/n SEWA MOBIL)</strong> untuk menyelesaikan pembayaran. dan untuk uang pembayaran denda
                        </p>
                        <p>Status akan berubah dalam 1x24 jam</p>
                        <br>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <a href="index.php?include=Pengembalian&data=<?php echo $id_transaksi?>" class="tm-btn tm-btn-default tm-right">Pengembalian</a>
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        <?php
            }
        ?>
        
<?php
    }
?>