<?php
    if(isset($_GET['data'])){
		$id_transaksi = $_GET['data'];
		$_SESSION['id_transaksi']=$id_transaksi;
		$sql_m = "select `T`.`id_transaksi`,`P`.`nama`,`Pa`.`nama_paket`,`Pa`.`harga`,`T`.`jumlah_produk`,
        `T`.`tanggal_pinjam`,`T`.`tanggal_kembali`,`T`.`lama`,`J`.`nama_jasa`,`J`.`harga_jasa`,`C`.`nama_cabang`,
        `T`.`total_harga`,`T`.`terlambat`,`T`.`denda`,`T`.`status_transaksi` ,`Pa`.`waktu` from `transaksi` `T` inner join `pelanggan` `P`
        ON `T`.`id_pelanggan`=`P`.`id_pelanggan` inner join `Paket` `Pa` ON `T`.`id_paket`=`Pa`.`id_paket` inner join
        `jasa` `J` ON `T`.`id_jasa`=`J`.`id_jasa` inner join `cabang` `C` ON `T`.`id_cabang`=`C`.`id_cabang` WHERE `T`.`id_transaksi` ='$id_transaksi'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_h = mysqli_fetch_array($query_m)){
			$id_transaksi = $data_h['id_transaksi'];
            $nama_pelanggan = $data_h['nama'];
            $nama_paket = $data_h['nama_paket'];
            $harga_paket = $data_h['harga'];
            $jumlah_produk = $data_h['jumlah_produk'];
            $tanggal_pinjaman = $data_h['tanggal_pinjam'];
            $tanggal_kembali = $data_h['tanggal_kembali'];
            $lama = $data_h['lama'];
            $nama_jasa = $data_h['nama_jasa'];
            $harga_jasa = $data_h['harga_jasa'];
            $nama_cabang = $data_h['nama_cabang'];
            $status = $data_h['status_transaksi'];
            $waktu = $data_h['waktu'];
            $total_harga=$data_h['total_harga'];

		} 
	}
?>
<header class="row tm-welcome-section">
    <h2 class="col-12 text-center tm-section-title">TRANSAKSI ANDA</h2>
    <p class="col-12 text-center">Terimakasih Sudah Melakukan Transaksi</p>
</header>
<div class="tm-container-inner tm-persons">
    <div class="mb-5"> 
        <table class="table table-bordered" style="color:black; border: 1px solid black; margin-left:auto;margin-right:auto" >
            <tbody>  
                <tr>
                    <td colspan="2">
                        <i class="fas fa-user-circle"></i> 
                        <strong>Detail Transaksi</strong>
                    </td>
                </tr>               
                <tr>
                    <td width="20%">
                        <strong>Nama Pelanggan:   </strong>
                    </td>
                    <td width="80%">
                        <?php echo $nama_pelanggan;?>
                    </td>
                </tr>                 
                <tr>
                    <td width="20%">
                        <strong>Jenis Paket:   </strong>
                    </td>
                    <td width="80%">
                        <?php echo $nama_paket;?>
                    </td>
                </tr>                 
                <tr>
                    <td width="20%">
                        <strong>Jumlah Produk:   </strong>
                    </td>
                    <td width="80%">
                        <?php echo $jumlah_produk;?>
                    </td>
                </tr> 
                <tr>
                    <td width="20%">
                        <strong>Tanggal Pinjaman:   <strong>
                    </td>
                    <td width="80%">
                        <?php echo $tanggal_pinjaman;?>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                        <strong>Lama Pinjaman:   </strong>
                    </td>
                    <td width="80%">
                        <?php echo $lama;?>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                        <strong>Tanggal Kembali:   </strong>
                    </td>
                    <td width="80%">
                        <?php echo $tanggal_kembali;?>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                        <strong>Jenis Jasa:   </strong>
                    </td>
                    <td width="80%">
                        <?php echo $nama_jasa;?>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                        <strong>Cabang:   </strong>
                    </td>
                    <td width="80%">
                        <?php echo $nama_cabang;?>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                        <strong>Total Harga:   </strong>
                    </td>
                    <td width="80%">
                        <?php 
                        $sql = "update `transaksi` set `total_harga`='$total_harga' where `id_transaksi`='$id_transaksi'";
                        mysqli_query($koneksi,$sql);
                        echo $total_harga;?>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                        <strong>Status:   </strong>
                    </td>
                    <td width="80%">
                        <?php echo $status;?>
                    </td>
                </tr>
            </tbody>
        </table> 
        <hr>
        <h3>Terimakasih</h3>
        <p>
            Transaksi persewaan anda telah berhasil<br>
            Silahkan anda membayar tagihan anda dengan cara transfer via Bank BRI di nomor Rekening : <br>
            <strong>(0986-01-025805-53-8 a/n SEWA MOBIL)</strong> untuk menyelesaikan pembayaran. dan untuk uang muka minimal setengah dari harga sewa.
        </p>
        <p>status akan berubah dalam 1x24 jam</p>
        <br>
        <div class="row">
            <div class="col-md-12 form-group">
                <a href="index.php?PRO=Profil&data=<?php echo $id_transaksi?>" class="tm-btn tm-btn-default tm-right">Konfirmasi</a>
            </div>
        </div>
    </div>
</div>