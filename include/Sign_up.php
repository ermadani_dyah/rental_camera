<header class="row tm-welcome-section">
    <h2 class="col-12 text-center tm-section-title">SignUp</h2>
    <p class="col-12 text-center">Masukan Data Diri Anda</p>
</header>
<div class="tm-container-inner tm-persons">
    <div class="row">
        <div class="col-md-12">
            <div class="form h-100">
                <h3>Get Started</h3>
                <div class="col-sm-10">
                    <?php   
                        if(!empty($_GET['notif'])){
                            if(!empty($_GET['jenis'])){
                                if($_GET['notif']=="tambahkosong"){
                    ?>
                                    <div class="alert alert-danger" role="alert">Maaf data
                                        <?php echo $_GET['jenis'];?> wajib di isi
                                    </div>
                    <?php        
                                }
                            }if($_GET['notif']=="tambahberhasil"){
                    ?>
                                <div class="alert alert-success" role="alert">
                                    Data Berhasil Ditambahkan
                                </div>          
                    <?php
                            }
                        }
                    ?>
                </div>
                <form class="mb-5" method="post" id="contactForm" name="contactForm" action="index.php?include=konfirmasi_tambah_pelanggan">
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Name Pelanggan *</label>
                            <input type="text" class="form-control" name="nama_pelanggan" id="nama_pelanggan"  value="<?php if(!empty($_SESSION['nama_pelanggan'])){ echo $_SESSION['nama_pelanggan'];} ?>">
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Alamat*</label>
                            <input type="text" class="form-control" name="alamat" id="alamat"  value="<?php if(!empty($_SESSION['alamat'])){ echo $_SESSION['alamat'];} ?>">
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Nomer HP *</label>
                            <input type="number" class="form-control" name="no_hp" id="no_hp" value="<?php if(!empty($_SESSION['no_hp'])){ echo $_SESSION['no_hp'];} ?>" >
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Email*</label>
                            <input type="email" class="form-control" name="email" id="email" value="<?php if(!empty($_SESSION['email'])){ echo $_SESSION['email'];} ?>">
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Nomer Identitas/KTP/SIM*</label>
                            <input type="number" class="form-control" name="identitas" id="identitas"  value="<?php if(!empty($_SESSION['identitas'])){ echo $_SESSION['identitas'];} ?>">
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Jenis Kelamin*</label>
                            <input type="text" class="form-control" name="jenis_kelamin" id="jenis_kelamin"  value="<?php if(!empty($_SESSION['jenis_kelamin'])){ echo $_SESSION['jenis_kelamin'];} ?>">
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Username*</label>
                            <input type="username" class="form-control" name="username" id="username"  value="<?php if(!empty($_SESSION['username'])){ echo $_SESSION['username'];} ?>">
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Password*</label>
                            <input type="password" class="form-control" name="password" id="password"  value="<?php if(!empty($_SESSION['password'])){ echo $_SESSION['password'];} ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <button type="submit" class="tm-btn tm-btn-default tm-right">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>