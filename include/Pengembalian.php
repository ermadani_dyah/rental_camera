<?php
    if(isset($_GET['data'])){
		$id_transaksi = $_GET['data'];
		$_SESSION['id_transaksi']=$id_transaksi;
		$sql_m = "select `P`.`nama` FROM  `transaksi` `T` INNER JOIN `pelanggan` `P` ON `T`.`id_pelanggan`=`P`.`id_pelanggan` where `T`.`id_transaksi` = '$id_transaksi'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
            $nama_pelanggan= $data_m[0];
		} 
	}
?>
<header class="row tm-welcome-section">
    <h2 class="col-12 text-center tm-section-title">Pengembalian Kamera</h2>
    <p class="col-12 text-center">Silahkan Lakukan Pengembalian Anda</p>
</header>
<div class="tm-container-inner tm-persons">
    <div class="row">
        <div class="col-md-12">
            <div class="form h-100">
                <h3>Get Started</h3>
                <div class="col-sm-10">
                    <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                        <?php if($_GET['notif']=="pengembaliankosong"){?>
                            <div class="alert alert-danger" role="alert">Maaf data
                                <?php echo $_GET['jenis'];?> wajib di isi
                            </div>
                        <?php }?>
                    <?php }?>
                </div>
                <form class="mb-5" enctype="multipart/form-data" method="post" id="contactForm" name="contactForm" action="index.php?include=konfirmasi_tambah_pengembalian">
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">ID Transaksi *</label>
                            <input type="text" class="form-control" name="nama_paket" id="nama_paket"  value="<?php echo $id_transaksi;?>">
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Name Pelanggan *</label>
                            <input type="text" class="form-control" name="nama_pelanggan" id="nama_pelanggan"  value="<?php echo $nama_pelanggan;?>">
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Tanggal Pengembalian Kembali *</label>
                            <input type="date" class="form-control" name="tanggal_kembali" id="tanggal_kembali" >
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Pembayaran Denda *</label>
                            <input type="file" class="form-control" name="foto" id="customFile">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <button type="submit" class="tm-btn tm-btn-default tm-right">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>