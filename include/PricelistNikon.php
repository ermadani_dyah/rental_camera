<header class="row tm-welcome-section">
	<h2 class="col-12 text-center tm-section-title">Nikon Camera</h2>
	<p class="col-12 text-center">At the Heart of the image</p>
</header>
<div class="tm-container-inner tm-persons">
	<div class="row">
		<?php
				$sql_h = "select `p`.`id_paket`,`pr`.`type`,`pr`.`spesifikasi`,`pr`.`kelengkapan` ,`p`.`nama_paket`, `m`.`nama_merek`,`p`.`waktu`,`p`.`harga`,`p`.`jumlah_produk`,`p`.`jenis_paket`,`p`.`status`,`pr`.`gambar` from `paket` `p`inner join `merek` `m` on `p`.`id_merek` = `m`.`id_merek` inner join `produk` `pr` on `p`.`id_produk`=`pr`.`id_produk` WHERE `m`.`nama_merek`='Nikon'"; 
				$sql_h .= "order by `nama_paket`";
				$query_h = mysqli_query($koneksi,$sql_h);
				while($data_h = mysqli_fetch_row($query_h)){
					$id_paket = $data_h[0];
					$type = $data_h[1];
					$spesifikasi = $data_h[2];
					$kelengkapan = $data_h[3];
					$nama_paket = $data_h[4];
					$merek = $data_h[5];
					$waktu = $data_h[6];
					$harga = $data_h[7];
					$jumlah_produk= $data_h[8];
					$jenis_paket= $data_h[9];
					$status= $data_h[10];
					$gambar= $data_h[11];
		?>
				<article class="col-lg-6">
					<figure class="tm-person">
						<img src="Admin/dist/img/<?php echo $gambar?>" alt="Image" class="img-fluid-2 tm-person-img" style="width:250px" />
						<figcaption class="tm-person-description">
							<h4 class="tm-person-name"><?php echo $merek?></h4>
							<p class="tm-person-title"><?php echo $type?></p>
							<p class="tm-person-about">Spesifikasi: <?php echo $spesifikasi?></p>
							<p class="tm-person-about">Kelengkapan: <?php echo $kelengkapan?></p>
							<p class="tm-person-about">Harga: <?php echo $harga.'/'.$waktu.' Jam'?></p>
							<p class="tm-person-about">Hari: <?php echo $jenis_paket?></p>
							<?php
								if($jumlah_produk>=1){
							?>
									<a href="index.php?LO=Login&data=<?php echo $id_paket;?>" class="tm-gallery-price"> <?php echo $status?></a>
							<?php
								}
								else if($jumlah_produk==0){
							?>
									<a href="#" class="tm-gallery-price" style="color:red;"> <?php echo $status?></a>
							<?php
								}
							?>
							<br><br>
							<div>
								<a href="https://fb.com" class="tm-social-link">
									<i class="fab fa-facebook tm-social-icon"></i>
								</a>
								<a href="https://twitter.com" class="tm-social-link">
									<i class="fab fa-twitter tm-social-icon"></i>
								</a>
								<a href="https://instagram.com" class="tm-social-link">
									<i class="fab fa-instagram tm-social-icon"></i>
								</a>
							</div>
						</figcaption>
					</figure>
				</article>
			<?php
				}
			?>
	</div>
</div>