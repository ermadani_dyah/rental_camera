<?php
	if(isset($_GET['data'])){
		$id_paket = $_GET['data'];
		$_SESSION['id_paket']=$id_paket;
		$sql_m = "select * from `paket` where `id_paket` = '$id_paket'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$id_paket= $data_m[0];
		}
	}
?>
<div class="container-fluid">
	<div class="row main-content bg-success text-center">
		<div class="col-md-4 text-center company__info" style="text-align: center;">
			<span class="company__logo">
				<img src="Admin/dist/img/lOGO.png" alt="">
			</span>
			<h4 class="company_title">Kamerain KUYY</h4>
		</div>
		<div class="col-md-8 col-xs-12 col-sm-12 login_form ">
			<div class="container-fluid">
				<div class="row" style="text-align: center;">
					<h2>Log In</h2>
				</div>
				<div class="row">
					<form control="" class="form-group" action="index.php?include=konfirmasi_login_profil" method="post">
						<?php if(!empty($_GET['gagal'])){?>
							<?php if($_GET['gagal']=="userKosong"){?>
								<span class="text-danger">Maaf Username Tidak Boleh Kosong</span>
							<?php } else if($_GET['gagal']=="passKosong"){?>
								<span class="text-danger">Maaf Password Tidak Boleh Kosong</span>
							<?php } else {?>
								<span class="text-danger">Maaf Username dan Password Anda Salah</span>
							<?php }?>
								<br>
						<?php }?>
						<?php if(!empty($_GET['notif'])){?>
							<?php if($_GET['notif']=="tambahberhasil"){?>
								<div class="alert alert-success" role="alert">
									Silahkan Lakukan Login
								</div>
							<?php }
							}
						?>	
						<div class="row">
							<input type="text" name="username" id="username" class="form__input" placeholder="Username">
						</div>
						<div class="row">
							<!-- <span class="fa fa-lock"></span> -->
							<input type="password" name="password" id="password" class="form__input" placeholder="Password">
						</div>
						<div class="row" style="text-align: center;">
							<button type="submit" name="submit" value="Submit" class="btn">Login</button>
						</div>
					</form>
				</div>
				<div class="row">
					<p>Don't have an account? <a href="index.php?include=Sign_up">Register Here</a></p>
				</div>
			</div>
		</div>
	</div>
</div>