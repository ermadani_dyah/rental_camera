<header class="row tm-welcome-section">
	<h2 class="col-12 text-center tm-section-title">Cabang Skuy Rental Camera</h2>
	<p class="col-12 text-center">Save Money, Live better</p>
</header>
<div class="tm-container-inner tm-persons">
	<div class="row">
		<?php
				$sql_h = "select * from `cabang`"; 
				$sql_h .= "order by `nama_cabang`";
				$query_h = mysqli_query($koneksi,$sql_h);
				while($data_h = mysqli_fetch_row($query_h)){
				$id_cabang = $data_h[0];
				$nama_cabang = $data_h[1];
				$alamat = $data_h[2];
		?>
				<article class="col-lg-7">
					<figure class="tm-person">
						<figcaption class="tm-person-description">
							<h4 class="tm-person-name"><?php echo $nama_cabang?></h4>
							<p class="tm-person-about"><?php echo $alamat?></p>
							<div>
								<a href="https://fb.com" class="tm-social-link">
									<i class="fab fa-facebook tm-social-icon"></i>
								</a>
								<a href="https://twitter.com" class="tm-social-link">
									<i class="fab fa-twitter tm-social-icon"></i>
								</a>
								<a href="https://instagram.com" class="tm-social-link">
									<i class="fab fa-instagram tm-social-icon"></i>
								</a>
							</div>
						</figcaption>
					</figure>
				</article>
			<?php
				}
			?>
	</div>
</div>