<header class="row tm-welcome-section">
    <h2 class="col-12 text-center tm-section-title">Skuy Rental Camera</h2>
    <p class="col-12 text-center">Dengan Kamera Kuy Kebutuhan Kamera Anda Akan Terpenuhi</p>
</header>

<div class="tm-paging-links">
    <nav>
        <ul>
            <li class="tm-paging-item">
                <a href="#" class="tm-paging-link active">Canon</a>
            </li>
            <li class="tm-paging-item">
            <a href="#" class="tm-paging-link">Nikon</a>
            </li>
            <li class="tm-paging-item">
                <a href="#" class="tm-paging-link">Sony</a>
            </li>
        </ul>
    </nav>
</div>
<!-- Gallery -->
<div class="row tm-gallery">
    <!-- gallery page 1 -->
    <div id="tm-gallery-page-canon" class="tm-gallery-page">
        <?php
            $sql_h = "select `p`.`id_produk`, `p`.`nama_produk`, `m`.`nama_merek`,`p`.`type`,`p`.`gambar`,`p`.`spesifikasi`,`p`.`kelengkapan` from `produk` `p`inner join `merek` `m` on `p`.`id_merek` = `m`.`id_merek` WHERE `m`.`nama_merek`='Canon'"; 
            $sql_h .= "order by `nama_produk`  ";
            $query_h = mysqli_query($koneksi,$sql_h);
            while($data_h = mysqli_fetch_row($query_h)){
                $id_produk = $data_h[0];
                $nama_produk = $data_h[1];
                $merek = $data_h[2];
                $type = $data_h[3];
                $gambar= $data_h[4];
                $spesifikasi = $data_h[5];
                $kelengkapan= $data_h[6];
            
        ?>
        <article class="col-lg-5 col-md-4 col-sm-6 col-12 tm-gallery-item">
            <figure>
                <img src="Admin/dist/img/<?php echo $gambar?>" alt="Image" class="img-fluid tm-gallery-img" />
                <figcaption>
                    <h4 class="tm-gallery-title"><?php echo $nama_produk.' '.$type?></h4>
                    <p class="tm-gallery-description"><?php echo $spesifikasi?></p>
                    <p class="tm-gallery-description"><?php echo $kelengkapan?></p>
                    <a href="index.php?include=PricelistCanon" class="tm-gallery-price tm-btn tm-btn-default tm-right">Pricelist</a>
                </figcaption>
            </figure>
        </article>
        <?php
            }
        ?>
    </div> <!-- gallery page 1 -->
    
    <!-- gallery page 2 -->
    <div id="tm-gallery-page-nikon" class="tm-gallery-page hidden">
        <?php
            $sql_h = "select `p`.`id_produk`, `p`.`nama_produk`, `m`.`nama_merek`,`p`.`type`,`p`.`gambar`,`p`.`spesifikasi`,`p`.`kelengkapan` from `produk` `p`inner join `merek` `m` on `p`.`id_merek` = `m`.`id_merek` WHERE `m`.`nama_merek`='Nikon'"; 
            $sql_h .= "order by `nama_produk`  ";
            $query_h = mysqli_query($koneksi,$sql_h);
            while($data_h = mysqli_fetch_row($query_h)){
                $id_produk = $data_h[0];
                $nama_produk = $data_h[1];
                $merek = $data_h[2];
                $type = $data_h[3];
                $gambar= $data_h[4];
                $spesifikasi = $data_h[5];
                $kelengkapan= $data_h[6];
            
        ?>
        <article class="col-lg-6 col-md-4 col-sm-6 col-12 tm-gallery-item">
            <figure>
                <img src="Admin/dist/img/<?php echo $gambar?>" alt="Image" class="img-fluid tm-gallery-img" />
                <figcaption>
                    <h4 class="tm-gallery-title"><?php echo $nama_produk.' '.$type?></h4>
                    <p class="tm-gallery-description"><?php echo $spesifikasi?></p>
                    <p class="tm-gallery-description"><?php echo $kelengkapan?></p>
                    <a href="index.php?include=PricelistNikon" class="tm-gallery-price tm-btn tm-btn-default tm-right">Pricelist</a>
                </figcaption>
            </figure>
        </article>
        <?php
            }
        ?>
    </div> <!-- gallery page 2 -->
    
    <!-- gallery page 3 -->
    <div id="tm-gallery-page-sony" class="tm-gallery-page hidden">
    <?php
            $sql_h = "select `p`.`id_produk`, `p`.`nama_produk`, `m`.`nama_merek`,`p`.`type`,`p`.`gambar`,`p`.`spesifikasi`,`p`.`kelengkapan` from `produk` `p`inner join `merek` `m` on `p`.`id_merek` = `m`.`id_merek` WHERE `m`.`nama_merek`='Sony'"; 
            $sql_h .= "order by `nama_produk`  ";
            $query_h = mysqli_query($koneksi,$sql_h);
            while($data_h = mysqli_fetch_row($query_h)){
                $id_produk = $data_h[0];
                $nama_produk = $data_h[1];
                $merek = $data_h[2];
                $type = $data_h[3];
                $gambar= $data_h[4];
                $spesifikasi = $data_h[5];
                $kelengkapan= $data_h[6];
            
        ?>
        <article class="col-lg-6 col-md-4 col-sm-6 col-12 tm-gallery-item">
            <figure>
                <img src="Admin/dist/img/<?php echo $gambar?>" alt="Image" class="img-fluid tm-gallery-img" />
                <figcaption>
                    <h4 class="tm-gallery-title"><?php echo $nama_produk.' '.$type?></h4>
                    <p class="tm-gallery-description"><?php echo $spesifikasi?></p>
                    <p class="tm-gallery-description"><?php echo $kelengkapan?></p>
                    <a href="index.php?include=PricelistSony" class="tm-gallery-price tm-btn tm-btn-default tm-right">Pricelist</a>
                </figcaption>
            </figure>
        </article>
        <?php
            }
        ?>
    </div> <!-- gallery page 3 -->
</div>