<!DOCTYPE html>
<?php
    if((isset($_GET['pelanggan']))&&(isset($_GET['paket']))){
		$id_pelanggan = $_GET['pelanggan'];
        $id_paket = $_GET['paket'];
		$_SESSION['id_pelanggan']=$id_pelanggan;
		$_SESSION['id_paket']=$id_paket;
		$sql_m = "select `nama`,`email`,`id_pelanggan` FROM  `pelanggan` where `id_pelanggan` = '$id_pelanggan'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$nama_pelanggan= $data_m[0];
            $email= $data_m[1];
            $id_pelanggan= $data_m[2];
		} 
        $sql_o = "select `nama_paket`,`id_paket` FROM  paket where `id_paket` = '$id_paket'";
		$query_o = mysqli_query($koneksi,$sql_o);
		while($data_o = mysqli_fetch_row($query_o)){
			$nama_paket= $data_o[0];
            $id_paket= $data_o[1];
		}
	}
?>
<header class="row tm-welcome-section">
    <h2 class="col-12 text-center tm-section-title">Sewa Kamera</h2>
    <p class="col-12 text-center">Silahkan Lakukan Transaksi Anda</p>
</header>
<div class="tm-container-inner tm-persons">
    <div class="row">
        <div class="col-md-12">
            <div class="form h-100">
                <h3>Get Started</h3>
                <div class="col-sm-10">
                    <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                        <?php if($_GET['notif']=="transaksikosong"){?>
                            <div class="alert alert-danger" role="alert">Maaf data
                                <?php echo $_GET['jenis'];?> wajib di isi
                            </div>
                        <?php }?>
                    <?php }?>
                </div>
                <form class="mb-5" method="post" id="contactForm" name="contactForm" action="index.php?include=konfirmasi_tambah_transaksi">
                    <div class="row">
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Name Pelanggan *</label>
                            <input type="text" class="form-control" name="nama_pelanggan" id="nama_pelanggan"  value="<?php echo $nama_pelanggan;?>">
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Nama Paket *</label>
                            <input type="text" class="form-control" name="nama_paket" id="nama_paket"  value="<?php echo $nama_paket;?>">
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Jumlah *</label>
                            <input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="Jumlah Yang Disewa" >
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Lama Pinjam/Jam*</label>
                            <input type="number" class="form-control" name="lama" id="lama" placeholder="Lama Pinjaman" >
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Tanggal Pinjam *</label>
                            <input type="date" class="form-control" name="tanggal_pinjam" id="tanggal_pinjam" >
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Tanggal Kembali *</label>
                            <input type="date" class="form-control" name="tanggal_kembali" id="tanggal_kembali" >
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Jasa *</label>
                            <select class="form-control" id="jasa" name="jasa">
                                <option value="0">- Pilih Jasa -</option>
                                <?php
                                $sql_j = "select * from `jasa` order by `id_jasa`";
                                $query_j = mysqli_query($koneksi,$sql_j);
                                        while($data_j = mysqli_fetch_row($query_j)){
                                            $id_jasa = $data_j[0];
                                            $nama_jasa = $data_j[1];
                                ?>
                                <option value="<?php echo $id_jasa;?>"<?php if(!empty($_SESSION['nama_jasa'])){if($id_jasa==$_SESSION['nama_jasa']){?> selected="selected" <?php }}?>>
                                    <?php echo $nama_jasa;?>
                                    <?php }?>
                                </option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group mb-3">
                            <label for="" class="col-form-label">Cabang *</label>
                            <select class="form-control" id="cabang" name="cabang">
                                <option value="0">- Pilih Jasa -</option>
                                <?php
                                $sql_j = "select * from `cabang` order by `id_cabang`";
                                $query_j = mysqli_query($koneksi,$sql_j);
                                        while($data_j = mysqli_fetch_row($query_j)){
                                            $id_cabang = $data_j[0];
                                            $nama_cabang = $data_j[1];
                                ?>
                                <option value="<?php echo $id_cabang;?>"<?php if(!empty($_SESSION['nama_cabang'])){if($id_cabang==$_SESSION['nama_jasa']){?> selected="selected" <?php }}?>>
                                    <?php echo $nama_cabang;?>
                                    <?php }?>
                                </option>
                            </select>
                        </div>
                    </div>
					<div class="row">
						<div class="col-md-12 form-group">
							<button type="submit" class="tm-btn tm-btn-default tm-right">Pesan</button>
						</div>
					</div>
                </form>
            </div>
        </div>
    </div>
</div>