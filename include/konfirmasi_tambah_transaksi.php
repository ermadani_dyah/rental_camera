<?php 
    if((isset($_SESSION['id_pelanggan']))&&(isset($_SESSION['id_paket']))){
        $id_pelanggan=$_SESSION['id_pelanggan'];
        $id_paket=$_SESSION['id_paket'];
        $jumlah_disewa = $_POST['jumlah'];
        $lama = $_POST['lama'];
        $pinjaman = $_POST['tanggal_pinjam'];
        $kembali = $_POST['tanggal_kembali'];
        $jasa = $_POST['jasa'];
        $cabang = $_POST['cabang'];
        $_SESSION['cabang']=$cabang;
        $_SESSION['jasa']=$jasa;
        $_SESSION['tanggal_kembali']=$kembali;
        $_SESSION['tanggal_pinjam']=$pinjaman;
        $_SESSION['lama']=$lama;
        $_SESSION['jumlah']=$jumlah_disewa;
        if(empty($cabang)){
            header("Location:index.php?include=Transaksi&data=".$cabang."&notif=transaksikosong&jenis=Nama Cabang");
        }else if(empty($jasa)){
            header("Location:index.php?include=Transaksi&data=".$jasa."&notif=transaksikosong&jenis=Jasa");
        }else if(empty($kembali)){
            header("Location:index.php?include=Transaksi&data=".$kembali."&notif=transaksikosong&jenis=Tanggal Kembali");
        }else if(empty($pinjaman)){
            header("Location:index.php?include=Transaksi&data=".$pinjaman."&notif=transaksikosong&jenis=Tanggal Pinjam");
        }else if(empty($lama)){
            header("Location:index.php?include=Transaksi&data=".$lama."&notif=transaksikosong&jenis=Lama Pinjam ");
        }else if(empty($jumlah_disewa)){
            header("Location:index.php?include=Transaksi&data=".$jumlah_disewa."&notif=transaksikosong&jenis=Jumlah Yang Disewa");
        }else{
            $sql_m = "select `harga`,`waktu` from `paket` where `id_paket` = '$id_paket'";
            $query_m = mysqli_query($koneksi,$sql_m);
            while($data_m = mysqli_fetch_row($query_m)){
                $harga_paket= $data_m[0];
                $waktu= $data_m[1];
            } 
            $sql_l = "select `harga_jasa` from `jasa` where `id_jasa` = '$jasa'";
            $query_l = mysqli_query($koneksi,$sql_l);
            while($data_l = mysqli_fetch_row($query_l)){
                $harga_jasa= $data_l[0];
            }
            
            (int)$sewa=(int)$lama/(int)$waktu;
            (int)$harga_kotor=(int)$harga_paket *(int)$sewa;
            (int)$total_harga =  (int)$harga_kotor*(int)$jumlah_disewa +(int)$harga_jasa;

            $sql = "insert into `transaksi` (`id_pelanggan`,`id_paket`,`jumlah_produk`,`tanggal_pinjam`,`tanggal_kembali`,`lama`,`id_jasa`,`id_cabang`,`total_harga`,`status_transaksi`)
            values ('$id_pelanggan','$id_paket','$jumlah_disewa','$pinjaman','$kembali','$lama','$jasa','$cabang','$total_harga','Belum Bayar')";
            mysqli_query($koneksi,$sql);
            
            $sql_o = "select `id_transaksi` from `transaksi` where `id_pelanggan` = '$id_pelanggan'";
            $query_o = mysqli_query($koneksi,$sql_o);
            while($data_o = mysqli_fetch_row($query_o)){
                $id_transaksi= $data_o[0];
            }
            header("Location:index.php?include=Detail_Transaksi&data=$id_transaksi");
            unset($_SESSION['cabang']);
            unset($_SESSION['jasa']);
            unset($_SESSION['tanggal_pinjam']);
            unset($_SESSION['tanggal_kembali']);
            unset($_SESSION['lama']);
            unset($_SESSION['jumlah']);
        }	
    }
?>
